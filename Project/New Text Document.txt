USE [JVGROUP]
GO
/****** Object:  Table [dbo].[Designation_Mst]    Script Date: 02/11/2011 18:22:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Designation_Mst](
	[DesignName] [varchar](100) COLLATE Latin1_General_CI_AI NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
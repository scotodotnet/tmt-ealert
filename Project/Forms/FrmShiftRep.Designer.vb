﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShiftRep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.label6 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.gvReport = New System.Windows.Forms.DataGridView
        Me.SINo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Date1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ShiftName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnView = New System.Windows.Forms.Button
        Me.cmbMacnoReg = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.gvReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label6
        '
        Me.label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.ForeColor = System.Drawing.Color.White
        Me.label6.Location = New System.Drawing.Point(1, 2)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(431, 25)
        Me.label6.TabIndex = 4
        Me.label6.Text = "EMPLOYEE SHIFT REPORT"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel1.Controls.Add(Me.cmbLocCode)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.cmbCompCode)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.gvReport)
        Me.Panel1.Controls.Add(Me.btnExit)
        Me.Panel1.Controls.Add(Me.btnView)
        Me.Panel1.Controls.Add(Me.cmbMacnoReg)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(2, 30)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(430, 374)
        Me.Panel1.TabIndex = 5
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(130, 39)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(279, 23)
        Me.cmbLocCode.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(10, 39)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 23)
        Me.Label13.TabIndex = 35
        Me.Label13.Text = "Location Code"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(130, 10)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(280, 23)
        Me.cmbCompCode.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(10, 10)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(118, 23)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "Company Code"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gvReport
        '
        Me.gvReport.AllowUserToAddRows = False
        Me.gvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvReport.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SINo, Me.Date1, Me.ShiftName})
        Me.gvReport.Location = New System.Drawing.Point(4, 138)
        Me.gvReport.Name = "gvReport"
        Me.gvReport.ReadOnly = True
        Me.gvReport.RowHeadersVisible = False
        Me.gvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvReport.Size = New System.Drawing.Size(422, 233)
        Me.gvReport.TabIndex = 31
        '
        'SINo
        '
        Me.SINo.HeaderText = "Si.No"
        Me.SINo.Name = "SINo"
        Me.SINo.ReadOnly = True
        Me.SINo.Width = 50
        '
        'Date1
        '
        Me.Date1.HeaderText = "Date"
        Me.Date1.Name = "Date1"
        Me.Date1.ReadOnly = True
        Me.Date1.Width = 150
        '
        'ShiftName
        '
        Me.ShiftName.HeaderText = "shift Name"
        Me.ShiftName.Name = "ShiftName"
        Me.ShiftName.ReadOnly = True
        Me.ShiftName.Width = 200
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.White
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(230, 106)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 28)
        Me.btnExit.TabIndex = 30
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnView
        '
        Me.btnView.BackColor = System.Drawing.Color.White
        Me.btnView.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnView.Location = New System.Drawing.Point(149, 106)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(75, 28)
        Me.btnView.TabIndex = 29
        Me.btnView.Text = "View"
        Me.btnView.UseVisualStyleBackColor = False
        '
        'cmbMacnoReg
        '
        Me.cmbMacnoReg.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbMacnoReg.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbMacnoReg.BackColor = System.Drawing.Color.White
        Me.cmbMacnoReg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMacnoReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMacnoReg.ForeColor = System.Drawing.Color.Black
        Me.cmbMacnoReg.FormattingEnabled = True
        Me.cmbMacnoReg.Location = New System.Drawing.Point(130, 68)
        Me.cmbMacnoReg.Name = "cmbMacnoReg"
        Me.cmbMacnoReg.Size = New System.Drawing.Size(280, 24)
        Me.cmbMacnoReg.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(10, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Employee No."
        '
        'FrmShiftRep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(434, 406)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.label6)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmShiftRep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Shift Rpeort"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.gvReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Private WithEvents cmbMacnoReg As System.Windows.Forms.ComboBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents btnExit As System.Windows.Forms.Button
    Private WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents gvReport As System.Windows.Forms.DataGridView
    Friend WithEvents SINo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Date1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ShiftName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class

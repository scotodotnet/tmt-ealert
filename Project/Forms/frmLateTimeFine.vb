﻿Public Class frmLateTimeFine

    Dim iStr1() As String
    Dim iStr2() As String

    Private Sub frmLateTimeFine_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        cmbCompCode.Enabled = False
        cmbCompCode_LostFocus(sender, e)
        cmbLocCode.SelectedIndex = 0

        Display_Data()




    End Sub
    Public Sub Display_Data()
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        SSQL = ""
        SSQL = "Select Late1,Late2,Late3,Fine1,Fine2,Fine3 From MstLateFine"
        SSQL = SSQL & " Where Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count > 0 Then
            txtLate1.Text = mDataSet.Tables(0).Rows(0)("Late1").ToString()
            txtLate2.Text = mDataSet.Tables(0).Rows(0)("Late2").ToString()
            txtLate3.Text = mDataSet.Tables(0).Rows(0)("Late3").ToString()

            txtFine1.Text = mDataSet.Tables(0).Rows(0)("Fine1").ToString()
            txtFine2.Text = mDataSet.Tables(0).Rows(0)("Fine2").ToString()
            txtFine3.Text = mDataSet.Tables(0).Rows(0)("Fine3").ToString()
        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")


        If Trim(txtLate1.Text) = "" Then
            MessageBox.Show("Please Enter the Late 1.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtLate1.Focus()
            Exit Sub
        End If
        If Trim(txtLate2.Text) = "" Then
            MessageBox.Show("Please Enter the Late 2.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtLate2.Focus()
            Exit Sub
        End If
        If Trim(txtLate3.Text) = "" Then
            MessageBox.Show("Please Enter the Late 3.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtLate3.Focus()
            Exit Sub
        End If

        If Trim(txtFine1.Text) = "" Then
            MessageBox.Show("Please Enter the Fine 3.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFine1.Focus()
            Exit Sub
        End If

        If Trim(txtFine1.Text) = "" Then
            MessageBox.Show("Please Enter the Fine 1.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFine1.Focus()
            Exit Sub
        End If

        If Trim(txtFine2.Text) = "" Then
            MessageBox.Show("Please Enter the Fine 2.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFine2.Focus()
            Exit Sub
        End If

        If Trim(txtFine3.Text) = "" Then
            MessageBox.Show("Please Enter the Fine 3.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFine3.Focus()
            Exit Sub
        End If

        Dim mSaveStatus As Long

        SSQL = ""
        SSQL = "Select Late1,Late2,Late3,Fine1,Fine2,Fine3 From MstLateFine"
        SSQL = SSQL & " Where Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count > 0 Then
            SSQL = ""
            SSQL = "Delete from MstLateFine"
            mSaveStatus = InsertDeleteUpdate(SSQL)
        End If

        SSQL = ""
        SSQL = "Insert into MstLateFine Values('" & txtLate1.Text & "','" & txtLate2.Text & "','" & txtLate3.Text & "', "
        SSQL = SSQL & "'" & txtFine1.Text & "','" & txtFine2.Text & "','" & txtFine3.Text & "','" & iStr1(0) & "','" & iStr2(0) & "')"
        mSaveStatus = InsertDeleteUpdate(SSQL)
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()

    End Sub
    Public Sub Clear()
        txtFine1.Text = ""
        txtFine2.Text = ""
        txtFine3.Text = ""
        txtLate1.Text = ""
        txtLate2.Text = ""
        txtLate3.Text = ""
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
     
        Clear()
    End Sub


End Class
﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Threading
Imports System.Globalization
#End Region

Public Class frmLogin
    Dim mStrComp() As String
    Dim mStrLoc() As String
    'Dim mDataset1 As Long
    Dim mDataset2 As New DataSet()
    Dim mserver As String
    Dim NextShift As String



    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)

        Dim appProc() As Process
        Dim strModName, strProcName As String

        strModName = Process.GetCurrentProcess.MainModule.ModuleName
        strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

        appProc = Process.GetProcessesByName(strProcName)

        ' ''If appProc.Length >= 1 Then
        ' ''    MessageBox.Show("There is an instance of this application running.")
        ' ''    End
        ' ''End If

        mUserLocation = ""

        Fill_Company_Details()
        mStrComp = Split(cmbCompCode.Text, " - ")
        Fill_Location_Details()
        mStrLoc = Split(cmbLocCode.Text, " - ")
        mCompCode = ""
        mLocCode = ""
        Get_Server_Date()
        Auto_update()
        mdiMain.btnCompany.Enabled = False
        mdiMain.btnLocation.Enabled = False
        mdiMain.btnDepartment.Enabled = False
        mdiMain.btnEmpType.Enabled = False
        mdiMain.btnShift.Enabled = False
        mdiMain.btnEmployee.Enabled = False
        mdiMain.btnIPDetails.Enabled = False
        mdiMain.btnMailAdd.Enabled = False
        mdiMain.btnMailSetup.Enabled = False
        mdiMain.btnReports.Enabled = False
        mdiMain.btnDownClear.Enabled = False
        mdiMain.btnSalDisb.Enabled = False
        mdiMain.btnDesignation.Enabled = False
        mdiMain.btnManAttn.Enabled = False
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnNfh.Enabled = False
        mdiMain.btnLogout.Enabled = False
        mdiMain.btnUserFPDownload.Enabled = False
        mdiMain.btnShiftmaster.Enabled = False
        mdiMain.btnUser.Enabled = False
        mdiMain.btnRights.Enabled = False

        'mdiMain.Hide() 'Mail Send that time mdi form hide
    End Sub

    Public Sub Get_Server_Date()
        SSQL = ""
        SSQL = "Select Convert(Varchar(10),GetDate(),103)"
        lblServerDate.Text = ReturnSingleValue(SSQL)
    End Sub

    Public Sub Fill_Location_Details()
        SSQL = ""
        SSQL = "Select LocCode + ' - ' + LocName From Location_Mst Where CompCode='" & Trim(mStrComp(0)) & "'"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbLocCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbLocCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbLocCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' - ' + CompName From Company_Mst"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbCompCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbCompCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbCompCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
    End Sub
    Private Sub Cancel_A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_A.Click
        mdiMain.btnCompany.Enabled = False
        mdiMain.btnLocation.Enabled = False
        mdiMain.btnDepartment.Enabled = False
        mdiMain.btnEmpType.Enabled = False
        mdiMain.btnShift.Enabled = False
        mdiMain.btnEmployee.Enabled = False
        mdiMain.btnIPDetails.Enabled = False
        mdiMain.btnMailAdd.Enabled = False
        mdiMain.btnMailSetup.Enabled = False
        mdiMain.btnReports.Enabled = False
        mdiMain.btnDownClear.Enabled = False
        mdiMain.btnSalDisb.Enabled = False
        mdiMain.btnDesignation.Enabled = False
        mdiMain.btnManAttn.Enabled = False
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnNfh.Enabled = False
        mdiMain.btnLogout.Enabled = False
        mdiMain.btnUser.Enabled = False
        mdiMain.btnRights.Enabled = False
        Me.Close()
    End Sub
    Private Sub Auto_update()
        Dim dt As New DataTable()
        Dim ds As New DataSet()
        Dim weekoff As String
        Dim dtserver As New DataTable()
        Dim value As String
        Dim ShiftType As String
        Dim ShiftTypeName As String
        Dim Shift1 As String
        Dim Shift2 As String
        Dim Shift3 As String
        Dim insertval As String
        Dim val As String = "0"
        Dim ServerDate As String
        Dim insertrec As String = "0"
        Dim mDataset1 As New DataSet()
        Dim mDataset3 As New DataSet()
        Dim dt_week As New DataSet()
        Dim mDataSet_4 As New DataSet()
        Dim mStatus As Long
        Dim ServerDt As String = "Select convert(varchar,getdate(),105) as dateFiedld"
        mserver = ReturnSingleValue(ServerDt).ToString()
        ServerDate = mserver.ToString()
        Dim Ssql As String = "Select MachineNo,EmpName,Category,ShiftName,ShiftType,NoDays,convert(varchar,NextDate,105) as NextDate,NextShift,AutoRotateShift,Shift1,Shift2,shift3,WeekOff,ShiftType,Ccode,Lcode from Shiftrrecording where Category='R' order by MachineNo Asc"
        mDataSet = ReturnMultipleValue(Ssql)
        con.Close()
        For i As Integer = 0 To mDataSet.Tables(0).Rows.Count - 1
            If Convert.ToDateTime(ServerDate) >= Convert.ToDateTime(mDataSet.Tables(0).Rows(i)("NextDate").ToString()) Then

                Dim q_val As String = "Select ID, ShiftType,First,Second,Third from ShiftAllocation where Value = '" & mDataSet.Tables(0).Rows(i)("ShiftType").ToString() & "' " & "and  (First='" & mDataSet.Tables(0).Rows(i)("ShiftName").ToString() & "' or Second = '" & mDataSet.Tables(0).Rows(i)("ShiftName").ToString() & "' " & " or Third='" & mDataSet.Tables(0).Rows(i)("ShiftName").ToString() & "')"
                mDataset1 = ReturnMultipleValue(q_val)
                'con.Open()
                'da2.Fill(dt1)
                'con.Close()
                If mDataSet.Tables(0).Rows(0)("AutoRotateShift") = "1" Then
                    If mDataset1.Tables(0).Rows(0)("First").ToString() = mDataSet.Tables(0).Rows(i)("ShiftName").ToString() Then
                        NextShift = mDataset1.Tables(0).Rows(0)("Second").ToString()
                    ElseIf mDataset1.Tables(0).Rows(0)("Second").ToString() = mDataSet.Tables(0).Rows(i)("ShiftName").ToString() Then
                        NextShift = mDataset1.Tables(0).Rows(0)("Third").ToString()
                    ElseIf mDataset1.Tables(0).Rows(0)("Third").ToString() = mDataSet.Tables(0).Rows(i)("ShiftName").ToString() Then
                        val = "1"
                        If mDataSet.Tables(0).Rows(0)("ShiftType") = "1" Then
                            ShiftType = "2"
                        ElseIf mDataSet.Tables(0).Rows(0)("ShiftType") = "2" Then
                            ShiftType = "3"
                        ElseIf mDataSet.Tables(0).Rows(0)("ShiftType") = "3" Then
                            ShiftType = "1"
                        End If
                        Dim qry_val_type As String = "select ShiftType from ShiftAllocation where Value='" & ShiftType & "'"
                        'Dim mDataset2.Tables(0) As New DataTable()
                        'Dim sda_name As New SqlDataAdapter(qry_val_type, con)
                        mDataset2 = ReturnMultipleValue(qry_val_type)
                        'con.Open()
                        'sda_name.Fill(mDataset2)
                        'con.Close()
                        ShiftTypeName = mDataset2.Tables(0).Rows(0)("ShiftType").ToString()
                        Dim qry_shiftval As String = "select First,Second,Third from ShiftAllocation where ShiftType='" & ShiftTypeName & "'"
                        mDataset3 = ReturnMultipleValue(qry_shiftval)
                        'Dim mDataset3.Tables(0) As New DataTable()
                        'Dim sda_type As New SqlDataAdapter(qry_shiftval, con)
                        'con.Open()
                        'sda_type.Fill(mDataset3.Tables(0))
                        'con.Close()
                        Shift1 = mDataset3.Tables(0).Rows(0)("First").ToString()
                        Shift2 = mDataset3.Tables(0).Rows(0)("Second").ToString()
                        Shift3 = mDataset3.Tables(0).Rows(0)("Third").ToString()
                    End If

                End If
                If val = "0" Then


                    If mDataset1.Tables(0).Rows(0)("First").ToString() = mDataSet.Tables(0).Rows(i)("NextShift").ToString() Then
                        NextShift = mDataset1.Tables(0).Rows(0)("Second").ToString()
                    ElseIf mDataset1.Tables(0).Rows(0)("Second").ToString() = mDataSet.Tables(0).Rows(i)("NextShift").ToString() Then
                        NextShift = mDataset1.Tables(0).Rows(0)("Third").ToString()
                    ElseIf mDataset1.Tables(0).Rows(0)("Third").ToString() = mDataSet.Tables(0).Rows(i)("NextShift").ToString() Then
                        NextShift = mDataset1.Tables(0).Rows(0)("First").ToString()
                    End If
                End If

                'Dim dt_week As New DataTable()
                Dim qry_week As String
                qry_week = "Select Weekoff,ID from WeekDays where Weekoff='" & mDataSet.Tables(0).Rows(i)("WeekOff").ToString() & "'"
                'Dim week_sda As New SqlDataAdapter(qry_week, con)
                'con.Open()
                'week_sda.Fill(dt_week)
                'con.Close()
                dt_week = ReturnMultipleValue(qry_week)
                If mDataSet.Tables(0).Rows(i)("NextShift").ToString() = "1" Then
                    If mDataSet.Tables(0).Rows(i)("Shift1").ToString() = "0" Then
                        weekoff = mDataSet.Tables(0).Rows(i)("WeekOff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift1").ToString() = "1" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) + 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift1").ToString() = "2" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) - 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                    End If
                ElseIf mDataSet.Tables(0).Rows(i)("NextShift").ToString() = "2" Then
                    If mDataSet.Tables(0).Rows(i)("Shift2").ToString() = "0" Then
                        weekoff = mDataSet.Tables(0).Rows(i)("WeekOff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift2").ToString() = "1" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) + 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift2").ToString() = "2" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) - 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                        'weekoff = dt_new.Rows(0)("Weekoff").ToString()
                    End If
                ElseIf mDataSet.Tables(0).Rows(i)("NextShift").ToString() = "3" Then
                    If mDataSet.Tables(0).Rows(i)("Shift3").ToString() = "0" Then
                        weekoff = mDataSet.Tables(0).Rows(i)("WeekOff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift3").ToString() = "1" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) + 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                    ElseIf mDataSet.Tables(0).Rows(i)("Shift3").ToString() = "2" Then
                        value = dt_week.Tables(0).Rows(0)("ID").ToString()
                        value = (Convert.ToInt32(value) - 1).ToString()
                        If value >= 8 Then
                            value = "1"
                        ElseIf value <= 0 Then
                            value = "7"
                        End If
                        Dim qry_newweekoff As String = "Select Weekoff,ID from WeekDays where ID='" & value & "'"
                        'Dim dt_new As New DataTable
                        'Dim sda_new As New SqlDataAdapter(qry_newweekoff, con)
                        'con.Open()
                        'sda_new.Fill(dt_new)
                        'con.Close()
                        'weekoff = dt_new.Rows(0)("Weekoff").ToString()
                        mDataSet_4 = ReturnMultipleValue(qry_newweekoff)
                        weekoff = mDataSet_4.Tables(0).Rows(0)("Weekoff").ToString()
                    End If
                End If

                Dim enddate As DateTime
                enddate = Convert.ToDateTime(DateTime.Today.AddDays(Convert.ToInt32(mDataSet.Tables(0).Rows(i)("NoDays").ToString())))
                Dim startday As String = DateTime.Today.DayOfWeek.ToString()
                If enddate.DayOfWeek.ToString() = weekoff.ToString() Then
                    enddate = enddate.AddDays(1)
                    startday = enddate.DayOfWeek.ToString()
                End If
                If val = "1" Then
                    insertval = "Update Shiftrrecording set ShiftName='" & Shift1 & "',ShiftType='" & ShiftType & "',StartingDate=convert(Datetime,'" & DateTime.Today & "',105), " & " NextDate=convert(datetime,'" & enddate & "',105),NextShift='" & Shift2 & "',WeekOff='" & weekoff & "',WeekStartDay='" & startday & "' where MachineNo='" & mDataSet.Tables(0).Rows(i)("MachineNo").ToString() & "' and Category = 'R' and Ccode='" & mDataSet.Tables(0).Rows(i)("Ccode").ToString() & "' and Lcode='" & mDataSet.Tables(0).Rows(i)("Lcode").ToString() & "'"
                    'cmd = New SqlCommand(insertval, con)
                    'con.Open()
                    'cmd.ExecuteNonQuery()
                    'con.Close()
                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode,Lcode) values ('" & mDataSet.Tables(0).Rows(i)("MachineNo").ToString() & "','" & mDataSet.Tables(0).Rows(i)("EmpName").ToString() & "','" & "R" & "','" & Shift1 & "','" & ShiftType & "','" & weekoff & "',convert(Datetime,'" & DateTime.Today & "',105),convert(datetime,'" & enddate & "',105),'" & mDataSet.Tables(0).Rows(i)("Ccode").ToString() & "','" & mDataSet.Tables(0).Rows(i)("Lcode").ToString() & "')"
                    'cmd = New SqlCommand(insertrec, con)
                    'con.Open()
                    'cmd.ExecuteNonQuery()
                    'con.Close()
                    mStatus = InsertDeleteUpdate(insertrec)
                Else
                    insertval = "Update Shiftrrecording set ShiftName = '" & mDataSet.Tables(0).Rows(i)("NextShift").ToString() & "'," & " StartingDate=convert(Datetime,'" & DateTime.Today & "',105), " & " NextDate=convert(datetime,'" & enddate & "',105),NextShift='" & NextShift & "',WeekOff='" & weekoff & "',WeekStartDay='" & startday & "' where MachineNo='" & mDataSet.Tables(0).Rows(i)("MachineNo").ToString() & "' and Category = 'R' and Ccode='" & mDataSet.Tables(0).Rows(i)("Ccode").ToString() & "' and Lcode='" & mDataSet.Tables(0).Rows(i)("Lcode").ToString() & "'"
                    ''cmd = New SqlCommand(insertval, con)
                    ''con.Open()
                    ''cmd.ExecuteNonQuery()
                    ''con.Close()
                    mStatus = InsertDeleteUpdate(insertval)
                    insertrec = "Insert into ShiftReport (MachineNo,EmpName,Category,Shiftname,ShiftType,Weekoff,StartingDate,NextDate,Ccode, Lcode) values ('" & mDataSet.Tables(0).Rows(i)("MachineNo").ToString() & "','" & mDataSet.Tables(0).Rows(i)("EmpName").ToString() & "','" & "R" & "','" & mDataSet.Tables(0).Rows(i)("NextShift").ToString() & "','" & mDataSet.Tables(0).Rows(i)("ShiftType").ToString() & "','" & weekoff & "',convert(Datetime,'" & DateTime.Today & "',105),convert(datetime,'" & enddate & "',105),'" & mDataSet.Tables(0).Rows(i)("Ccode").ToString() & "','" & mDataSet.Tables(0).Rows(i)("Lcode").ToString() & "')"
                    'cmd = New SqlCommand(insertrec, con)
                    'con.Open()
                    'cmd.ExecuteNonQuery()
                    'con.Close()
                    mStatus = InsertDeleteUpdate(insertrec)
                End If



            End If
        Next
    End Sub

    Private Sub OK_A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_A.Click
        mvarLoginUser = ""
        mUserLocation = ""


        'Check Date Value Start
        mvarLoginUser = ""
        mUserLocation = ""
        Dim q As String = ""
        'Dim Dt As DataTable = New DataTable()
        SSQL = "Select * from Verification_Mst"
        q = ReturnSingleValue(SSQL)
        Dim Verification_Date As String
        Dim Verification_Date1 As String
        If q = "" Then
            SSQL = "Select Col_M1,Col_d1,Col_Y1 from Value_Mst"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                Verification_Date = (5 & "-" & 9 & "-" & 2020).ToString()
                SSQL = "select convert(varchar,getdate(),105)"
                Verification_Date1 = ReturnSingleValue(SSQL)
                If (Convert.ToDateTime(Verification_Date) >= Convert.ToDateTime(Verification_Date1)) Then
                    'Skip
                Else
                    Dim Ins As String = "Insert Into Verification_Mst (Value1) values ('redf#sdfdsfdsf9890834==')"
                    InsertDeleteUpdate(Ins)
                    MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Return
                    'Me.Close()
                    'Application.Exit()
                    Exit Sub
                End If

            Else
                MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'Return
                'Application.Exit()
                Exit Sub
            End If
        Else
            'MessageBox.Show("Your Licence Key has been Expired. Update your Licence Key", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MessageBox.Show("Date Expired....", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Return
            'Application.Exit()
            Exit Sub
        End If
        'Check Date Value End


        If Trim(txtAdminName.Text) = "" Then
            MessageBox.Show("Please enter user name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If
        If Trim(txtAdminPwd.Text) = "" Then
            MessageBox.Show("Please enter password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminPwd.Focus()
            Exit Sub
        End If

        SSQL = ""
        SSQL = "Select Count(UserName) from Admin_Login "
        SSQL = SSQL & " Where UserName='" & UCase(Trim(txtAdminName.Text)) & "'"
        SSQL = SSQL & " And Password='" & Trim(Encryption(UCase(Trim(txtAdminPwd.Text)))) & "'"

        Dim mRetVal As Integer
        mRetVal = ReturnSingleValue(SSQL)
        'mRetVal = 1
        If mRetVal <= 0 Then
            Call OK_U_Click(sender, e)
            'MessageBox.Show("Please try again.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If

        mdiMain.btnDepartment.Enabled = True
        mdiMain.btnEmpType.Enabled = True
        mdiMain.btnShift.Enabled = True
        mdiMain.btnEmployee.Enabled = True
        mdiMain.btnIPDetails.Enabled = True
        mdiMain.btnMailAdd.Enabled = True
        mdiMain.btnMailSetup.Enabled = True
        mdiMain.btnReports.Enabled = True
        mdiMain.btnDownClear.Enabled = True
        mdiMain.btnSalDisb.Enabled = True
        mdiMain.btnDesignation.Enabled = True
        mdiMain.btnManAttn.Enabled = True
        mdiMain.btnMachineData.Enabled = True
        mdiMain.statusLocation.Text = "ALL"
        mdiMain.btnShiftmaster.Enabled = True
        mdiMain.btnUserFPDownload.Enabled = True
        mdiMain.btnNfh.Enabled = True
        mdiMain.btnLogout.Enabled = True
        mdiMain.btnUser.Enabled = True
        mdiMain.btnRights.Enabled = True
        mdiMain.btnMaster.Enabled = True
        mdiMain.btnGraceTime.Enabled = True
        mdiMain.statusUser.Text = UCase(Trim(txtAdminName.Text))
        mdiMain.statusServerDate.Text = lblServerDate.Text
        mvarLoginUser = UCase(Trim(txtAdminName.Text))
        Me.Close()
        'mdiMain.Hide() Mail Send that time mdi form hide
    End Sub

    Private Sub OK_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_U.Click
        mvarLoginUser = ""
        mUserLocation = ""

        txtUserName.Text = txtAdminName.Text
        txtUserPwd.Text = txtAdminPwd.Text
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If
        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select location details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        If Trim(txtAdminName.Text) = "" Then
            MessageBox.Show("Please enter user name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If
        If Trim(txtAdminPwd.Text) = "" Then
            MessageBox.Show("Please enter user password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminPwd.Focus()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim istr2() As String
        Dim passtr As String

        passtr = Trim(Encryption(UCase(Trim(txtAdminPwd.Text)))).ToString()


        iStr1 = Trim(cmbCompCode.Text).Split("-")
        istr2 = Trim(cmbLocCode.Text).Split("-")
        SSQL = ""
        SSQL = "Select Count(UserName) from User_Login "
        SSQL = SSQL & " Where CompCode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " And LocCode='" & Trim(istr2(0)) & "'"
        SSQL = SSQL & " And UserName='" & UCase(Trim(txtAdminName.Text)) & "'"
        SSQL = SSQL & " And Password='" & Trim(Encryption(UCase(Trim(txtAdminPwd.Text)))) & "'"

        Dim mRetVal As Integer

        mRetVal = ReturnSingleValue(SSQL)
        If mRetVal <= 0 Then
            MessageBox.Show("Please try again.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtAdminName.Focus()
            Exit Sub
        End If

        mCompCode = cmbCompCode.Text
        mLocCode = cmbLocCode.Text
        Dim qry As String = ""
        qry = "Select Data from Rights where CompCode = '" & Trim(iStr1(0)) & "' and LocCode = '" & Trim(istr2(0)) & "' and Username = '" & UCase(Trim(txtUserName.Text)) & "'"
        mDataSet = ReturnMultipleValue(qry)
        If mDataSet.Tables(0).Rows.Count > 0 Then
            mCompCode = cmbCompCode.Text
            mLocCode = cmbLocCode.Text
            Dim Dept() As String
            Dept = Split(mDataSet.Tables(0).Rows(0)("Data").ToString(), ",")
            If Dept(0) = "1" Then
                mdiMain.btnDepartment.Enabled = True
            Else
                mdiMain.btnDepartment.Enabled = False
            End If
            If Dept(1) = "1" Then
                mdiMain.btnDesignation.Enabled = True
            Else
                mdiMain.btnDesignation.Enabled = False
            End If
            If Dept(2) = "1" Then
                mdiMain.btnDownClear.Enabled = True
            Else
                mdiMain.btnDownClear.Enabled = False
            End If
            If Dept(3) = "1" Then
                mdiMain.btnMailSetup.Enabled = True
            Else
                mdiMain.btnMailSetup.Enabled = False
            End If
            If Dept(4) = "1" Then
                mdiMain.btnEmployee.Enabled = True
            Else
                mdiMain.btnEmployee.Enabled = False
            End If
            If Dept(5) = "1" Then
                mdiMain.btnEmpType.Enabled = True
            Else
                mdiMain.btnEmpType.Enabled = False
            End If
            If Dept(6) = "1" Then
                mdiMain.btnIPDetails.Enabled = True
            Else
                mdiMain.btnIPDetails.Enabled = False
            End If
            If Dept(7) = "1" Then
                mdiMain.btnMachineData.Enabled = True
            Else
                mdiMain.btnMachineData.Enabled = False
            End If
            If Dept(8) = "1" Then
                mdiMain.btnMailAdd.Enabled = True
            Else
                mdiMain.btnMailAdd.Enabled = False
            End If
            If Dept(9) = "1" Then
                mdiMain.btnManAttn.Enabled = True
            Else
                mdiMain.btnManAttn.Enabled = False
            End If
            If Dept(10) = "1" Then
                mdiMain.btnReports.Enabled = True
            Else
                mdiMain.btnReports.Enabled = False
            End If
            If Dept(11) = "1" Then
                mdiMain.btnSalDisb.Enabled = True
            Else
                mdiMain.btnSalDisb.Enabled = False
            End If
            If Dept(12) = "1" Then
                'mdiMain.btnStaff.Enabled = True
            Else
                'mdiMain.btnStaff.Enabled = False
            End If
            If Dept(13) = "1" Then
                mdiMain.btnUser.Enabled = True
            Else
                mdiMain.btnUser.Enabled = False
            End If

            If Dept(14) = "1" Then
                'mdiMain.btnNoShift.Enabled = True
            Else
                'mdiMain.btnNoShift.Enabled = False
            End If
            If Dept(15) = "1" Then
                mdiMain.btnShift.Enabled = True
            Else
                mdiMain.btnShift.Enabled = False
            End If
            If Dept(16) = "1" Then
                mdiMain.btnUserFPDownload.Enabled = True
            Else
                mdiMain.btnUserFPDownload.Enabled = False
            End If
            If Dept(17) = "1" Then
                mdiMain.btnShiftmaster.Enabled = True
            Else
                mdiMain.btnShiftmaster.Enabled = False
            End If
            mdiMain.btnLogout.Enabled = True
            mdiMain.btnGraceTime.Enabled = True
            mdiMain.statusLocation.Text = UCase(Trim(istr2(0)))
            mdiMain.statusUser.Text = UCase(Trim(txtUserName.Text))
            mdiMain.statusServerDate.Text = lblServerDate.Text
            mvarLoginUser = UCase(Trim(txtUserName.Text))
            mUserLocation = Trim(istr2(0))
            Me.Close()
            'mdiMain.Hide() Mail Send that time mdi form hide

        End If
    End Sub

    Private Sub Cancel_U_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_U.Click
        mdiMain.btnCompany.Enabled = False
        mdiMain.btnLocation.Enabled = False
        mdiMain.btnDepartment.Enabled = False
        mdiMain.btnEmpType.Enabled = False
        mdiMain.btnShift.Enabled = False
        mdiMain.btnEmployee.Enabled = False
        mdiMain.btnIPDetails.Enabled = False
        mdiMain.btnMailAdd.Enabled = False
        mdiMain.btnMailSetup.Enabled = False
        mdiMain.btnReports.Enabled = False
        mdiMain.btnDownClear.Enabled = False
        mdiMain.btnSalDisb.Enabled = False
        mdiMain.btnDesignation.Enabled = False
        mdiMain.btnManAttn.Enabled = False
        mdiMain.btnMachineData.Enabled = False
        mdiMain.btnUser.Enabled = False
        mdiMain.btnRights.Enabled = False
        mdiMain.btnMaster.Enabled = False
        Me.Close()
    End Sub

    Private Sub LogoPictureBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub lblServerDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblServerDate.Click

    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mdiMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mdiMain))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.statusLocation = New System.Windows.Forms.ToolStripStatusLabel
        Me.statusUser = New System.Windows.Forms.ToolStripStatusLabel
        Me.statusServerDate = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnCompany = New System.Windows.Forms.Button
        Me.btnLocation = New System.Windows.Forms.Button
        Me.btnDepartment = New System.Windows.Forms.Button
        Me.btnEmpType = New System.Windows.Forms.Button
        Me.btnShift = New System.Windows.Forms.Button
        Me.btnEmployee = New System.Windows.Forms.Button
        Me.btnIPDetails = New System.Windows.Forms.Button
        Me.btnMailAdd = New System.Windows.Forms.Button
        Me.btnMailSetup = New System.Windows.Forms.Button
        Me.btnReports = New System.Windows.Forms.Button
        Me.btnDownClear = New System.Windows.Forms.Button
        Me.btnSalDisb = New System.Windows.Forms.Button
        Me.btnDesignation = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.btnManAttn = New System.Windows.Forms.Button
        Me.btnMachineData = New System.Windows.Forms.Button
        Me.AxSB100PC1 = New Axzkemkeeper.AxCZKEM
        Me.btnShiftmaster = New System.Windows.Forms.Button
        Me.btnUserFPDownload = New System.Windows.Forms.Button
        Me.btnNfh = New System.Windows.Forms.Button
        Me.btnLogout = New System.Windows.Forms.Button
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.fg = New C1.Win.C1FlexGrid.Classic.C1FlexGridClassic
        Me.btnUser = New System.Windows.Forms.Button
        Me.btnRights = New System.Windows.Forms.Button
        Me.btnMaster = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnGraceTime = New System.Windows.Forms.Button
        Me.StatusStrip.SuspendLayout()
        CType(Me.AxSB100PC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel, Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.statusLocation, Me.statusUser, Me.statusServerDate})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 722)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1028, 24)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(136, 19)
        Me.ToolStripStatusLabel.Text = " eAlert - Admin Module"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(130, 19)
        Me.ToolStripStatusLabel1.Text = "Scoto Systems Pvt Ltd."
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(59, 19)
        Me.ToolStripStatusLabel2.Text = "LTL MILL"
        '
        'statusLocation
        '
        Me.statusLocation.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.statusLocation.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.statusLocation.Name = "statusLocation"
        Me.statusLocation.Size = New System.Drawing.Size(57, 19)
        Me.statusLocation.Text = "Location"
        '
        'statusUser
        '
        Me.statusUser.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.statusUser.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.statusUser.Name = "statusUser"
        Me.statusUser.Size = New System.Drawing.Size(34, 19)
        Me.statusUser.Text = "User"
        '
        'statusServerDate
        '
        Me.statusServerDate.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.statusServerDate.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.statusServerDate.Name = "statusServerDate"
        Me.statusServerDate.Size = New System.Drawing.Size(70, 19)
        Me.statusServerDate.Text = "Server Date"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Maroon
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1028, 3)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'btnCompany
        '
        Me.btnCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompany.Location = New System.Drawing.Point(5, 13)
        Me.btnCompany.Name = "btnCompany"
        Me.btnCompany.Size = New System.Drawing.Size(172, 29)
        Me.btnCompany.TabIndex = 12
        Me.btnCompany.Text = "Company"
        Me.btnCompany.UseVisualStyleBackColor = True
        '
        'btnLocation
        '
        Me.btnLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLocation.Location = New System.Drawing.Point(5, 48)
        Me.btnLocation.Name = "btnLocation"
        Me.btnLocation.Size = New System.Drawing.Size(172, 29)
        Me.btnLocation.TabIndex = 12
        Me.btnLocation.Text = "Location"
        Me.btnLocation.UseVisualStyleBackColor = True
        '
        'btnDepartment
        '
        Me.btnDepartment.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDepartment.Location = New System.Drawing.Point(4, 188)
        Me.btnDepartment.Name = "btnDepartment"
        Me.btnDepartment.Size = New System.Drawing.Size(172, 29)
        Me.btnDepartment.TabIndex = 12
        Me.btnDepartment.Text = "Department"
        Me.btnDepartment.UseVisualStyleBackColor = True
        '
        'btnEmpType
        '
        Me.btnEmpType.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmpType.Location = New System.Drawing.Point(5, 223)
        Me.btnEmpType.Name = "btnEmpType"
        Me.btnEmpType.Size = New System.Drawing.Size(172, 29)
        Me.btnEmpType.TabIndex = 12
        Me.btnEmpType.Text = "Employee Type"
        Me.btnEmpType.UseVisualStyleBackColor = True
        '
        'btnShift
        '
        Me.btnShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShift.Location = New System.Drawing.Point(493, 77)
        Me.btnShift.Name = "btnShift"
        Me.btnShift.Size = New System.Drawing.Size(172, 29)
        Me.btnShift.TabIndex = 12
        Me.btnShift.Text = "Shift Type Details"
        Me.btnShift.UseVisualStyleBackColor = True
        Me.btnShift.Visible = False
        '
        'btnEmployee
        '
        Me.btnEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployee.Location = New System.Drawing.Point(4, 291)
        Me.btnEmployee.Name = "btnEmployee"
        Me.btnEmployee.Size = New System.Drawing.Size(172, 29)
        Me.btnEmployee.TabIndex = 12
        Me.btnEmployee.Text = "Employee Details"
        Me.btnEmployee.UseVisualStyleBackColor = True
        '
        'btnIPDetails
        '
        Me.btnIPDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIPDetails.Location = New System.Drawing.Point(5, 153)
        Me.btnIPDetails.Name = "btnIPDetails"
        Me.btnIPDetails.Size = New System.Drawing.Size(172, 29)
        Me.btnIPDetails.TabIndex = 12
        Me.btnIPDetails.Text = "Time Delete"
        Me.btnIPDetails.UseVisualStyleBackColor = True
        '
        'btnMailAdd
        '
        Me.btnMailAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMailAdd.Location = New System.Drawing.Point(353, 232)
        Me.btnMailAdd.Name = "btnMailAdd"
        Me.btnMailAdd.Size = New System.Drawing.Size(172, 29)
        Me.btnMailAdd.TabIndex = 12
        Me.btnMailAdd.Text = "Mailing Address"
        Me.btnMailAdd.UseVisualStyleBackColor = True
        Me.btnMailAdd.Visible = False
        '
        'btnMailSetup
        '
        Me.btnMailSetup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMailSetup.Location = New System.Drawing.Point(353, 276)
        Me.btnMailSetup.Name = "btnMailSetup"
        Me.btnMailSetup.Size = New System.Drawing.Size(172, 29)
        Me.btnMailSetup.TabIndex = 12
        Me.btnMailSetup.Text = "eMail Setup"
        Me.btnMailSetup.UseVisualStyleBackColor = True
        Me.btnMailSetup.Visible = False
        '
        'btnReports
        '
        Me.btnReports.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReports.Location = New System.Drawing.Point(3, 359)
        Me.btnReports.Name = "btnReports"
        Me.btnReports.Size = New System.Drawing.Size(172, 29)
        Me.btnReports.TabIndex = 12
        Me.btnReports.Text = "Reports"
        Me.btnReports.UseVisualStyleBackColor = True
        '
        'btnDownClear
        '
        Me.btnDownClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownClear.Location = New System.Drawing.Point(5, 118)
        Me.btnDownClear.Name = "btnDownClear"
        Me.btnDownClear.Size = New System.Drawing.Size(172, 29)
        Me.btnDownClear.TabIndex = 12
        Me.btnDownClear.Text = "Download / Clear"
        Me.btnDownClear.UseVisualStyleBackColor = True
        '
        'btnSalDisb
        '
        Me.btnSalDisb.Enabled = False
        Me.btnSalDisb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalDisb.Location = New System.Drawing.Point(2, 553)
        Me.btnSalDisb.Name = "btnSalDisb"
        Me.btnSalDisb.Size = New System.Drawing.Size(172, 29)
        Me.btnSalDisb.TabIndex = 12
        Me.btnSalDisb.Text = "Salary Disbursement"
        Me.btnSalDisb.UseVisualStyleBackColor = True
        Me.btnSalDisb.Visible = False
        '
        'btnDesignation
        '
        Me.btnDesignation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDesignation.Location = New System.Drawing.Point(4, 258)
        Me.btnDesignation.Name = "btnDesignation"
        Me.btnDesignation.Size = New System.Drawing.Size(172, 29)
        Me.btnDesignation.TabIndex = 12
        Me.btnDesignation.Text = "Designation Details"
        Me.btnDesignation.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox5.Location = New System.Drawing.Point(2, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(180, 3)
        Me.GroupBox5.TabIndex = 19
        Me.GroupBox5.TabStop = False
        '
        'btnManAttn
        '
        Me.btnManAttn.Enabled = False
        Me.btnManAttn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManAttn.Location = New System.Drawing.Point(4, 326)
        Me.btnManAttn.Name = "btnManAttn"
        Me.btnManAttn.Size = New System.Drawing.Size(172, 29)
        Me.btnManAttn.TabIndex = 12
        Me.btnManAttn.Text = "Manual Attendance"
        Me.btnManAttn.UseVisualStyleBackColor = True
        '
        'btnMachineData
        '
        Me.btnMachineData.Enabled = False
        Me.btnMachineData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMachineData.Location = New System.Drawing.Point(5, 588)
        Me.btnMachineData.Name = "btnMachineData"
        Me.btnMachineData.Size = New System.Drawing.Size(172, 29)
        Me.btnMachineData.TabIndex = 12
        Me.btnMachineData.Text = "Machine Data"
        Me.btnMachineData.UseVisualStyleBackColor = True
        Me.btnMachineData.Visible = False
        '
        'AxSB100PC1
        '
        Me.AxSB100PC1.Enabled = True
        Me.AxSB100PC1.Location = New System.Drawing.Point(130, 432)
        Me.AxSB100PC1.Name = "AxSB100PC1"
        Me.AxSB100PC1.OcxState = CType(resources.GetObject("AxSB100PC1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxSB100PC1.Size = New System.Drawing.Size(47, 47)
        Me.AxSB100PC1.TabIndex = 21
        '
        'btnShiftmaster
        '
        Me.btnShiftmaster.Enabled = False
        Me.btnShiftmaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShiftmaster.Location = New System.Drawing.Point(261, 102)
        Me.btnShiftmaster.Name = "btnShiftmaster"
        Me.btnShiftmaster.Size = New System.Drawing.Size(172, 29)
        Me.btnShiftmaster.TabIndex = 23
        Me.btnShiftmaster.Text = "Shift Master"
        Me.btnShiftmaster.UseVisualStyleBackColor = True
        Me.btnShiftmaster.Visible = False
        '
        'btnUserFPDownload
        '
        Me.btnUserFPDownload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserFPDownload.Location = New System.Drawing.Point(0, 553)
        Me.btnUserFPDownload.Name = "btnUserFPDownload"
        Me.btnUserFPDownload.Size = New System.Drawing.Size(172, 29)
        Me.btnUserFPDownload.TabIndex = 25
        Me.btnUserFPDownload.Text = "Manual OT"
        Me.btnUserFPDownload.UseVisualStyleBackColor = True
        Me.btnUserFPDownload.Visible = False
        '
        'btnNfh
        '
        Me.btnNfh.Enabled = False
        Me.btnNfh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNfh.Location = New System.Drawing.Point(5, 629)
        Me.btnNfh.Name = "btnNfh"
        Me.btnNfh.Size = New System.Drawing.Size(172, 29)
        Me.btnNfh.TabIndex = 27
        Me.btnNfh.Text = "Holiday Master"
        Me.btnNfh.UseVisualStyleBackColor = True
        Me.btnNfh.Visible = False
        '
        'btnLogout
        '
        Me.btnLogout.Enabled = False
        Me.btnLogout.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.Location = New System.Drawing.Point(3, 394)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(172, 29)
        Me.btnLogout.TabIndex = 28
        Me.btnLogout.Text = "Logout"
        Me.btnLogout.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'fg
        '
        Me.fg.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.FixedSingle
        Me.fg.ColumnInfo = "10,0,0,0,0,85,Columns:"
        Me.fg.FixedCols = 0
        Me.fg.GridColorFixed = System.Drawing.SystemColors.ControlDark
        Me.fg.Location = New System.Drawing.Point(363, 327)
        Me.fg.Name = "fg"
        Me.fg.NodeClosedPicture = Nothing
        Me.fg.NodeOpenPicture = Nothing
        Me.fg.OutlineCol = -1
        Me.fg.Size = New System.Drawing.Size(302, 160)
        Me.fg.TabIndex = 30
        Me.fg.TreeColor = System.Drawing.Color.DarkGray
        Me.fg.Visible = False
        '
        'btnUser
        '
        Me.btnUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUser.Location = New System.Drawing.Point(4, 12)
        Me.btnUser.Name = "btnUser"
        Me.btnUser.Size = New System.Drawing.Size(172, 29)
        Me.btnUser.TabIndex = 32
        Me.btnUser.Text = "Company Master"
        Me.btnUser.UseVisualStyleBackColor = True
        '
        'btnRights
        '
        Me.btnRights.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRights.Location = New System.Drawing.Point(5, 47)
        Me.btnRights.Name = "btnRights"
        Me.btnRights.Size = New System.Drawing.Size(172, 29)
        Me.btnRights.TabIndex = 33
        Me.btnRights.Text = "User Rights"
        Me.btnRights.UseVisualStyleBackColor = True
        '
        'btnMaster
        '
        Me.btnMaster.Enabled = False
        Me.btnMaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMaster.Location = New System.Drawing.Point(212, 374)
        Me.btnMaster.Name = "btnMaster"
        Me.btnMaster.Size = New System.Drawing.Size(172, 29)
        Me.btnMaster.TabIndex = 35
        Me.btnMaster.Text = "Master"
        Me.btnMaster.UseVisualStyleBackColor = True
        Me.btnMaster.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.BackgroundImage = Global.eAlert_Admin.My.Resources.Resources.admin
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Location = New System.Drawing.Point(5, 429)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(172, 50)
        Me.PictureBox2.TabIndex = 14
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Location = New System.Drawing.Point(0, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(182, 719)
        Me.PictureBox1.TabIndex = 11
        Me.PictureBox1.TabStop = False
        '
        'btnGraceTime
        '
        Me.btnGraceTime.Enabled = False
        Me.btnGraceTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGraceTime.Location = New System.Drawing.Point(5, 83)
        Me.btnGraceTime.Name = "btnGraceTime"
        Me.btnGraceTime.Size = New System.Drawing.Size(172, 29)
        Me.btnGraceTime.TabIndex = 37
        Me.btnGraceTime.Text = "Grace Time"
        Me.btnGraceTime.UseVisualStyleBackColor = True
        '
        'mdiMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 746)
        Me.Controls.Add(Me.btnGraceTime)
        Me.Controls.Add(Me.btnMaster)
        Me.Controls.Add(Me.btnRights)
        Me.Controls.Add(Me.btnUser)
        Me.Controls.Add(Me.fg)
        Me.Controls.Add(Me.btnLogout)
        Me.Controls.Add(Me.btnNfh)
        Me.Controls.Add(Me.btnUserFPDownload)
        Me.Controls.Add(Me.btnShiftmaster)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.btnDownClear)
        Me.Controls.Add(Me.btnMachineData)
        Me.Controls.Add(Me.btnReports)
        Me.Controls.Add(Me.btnSalDisb)
        Me.Controls.Add(Me.btnMailSetup)
        Me.Controls.Add(Me.btnMailAdd)
        Me.Controls.Add(Me.btnIPDetails)
        Me.Controls.Add(Me.btnManAttn)
        Me.Controls.Add(Me.btnEmployee)
        Me.Controls.Add(Me.btnDesignation)
        Me.Controls.Add(Me.btnShift)
        Me.Controls.Add(Me.btnEmpType)
        Me.Controls.Add(Me.btnDepartment)
        Me.Controls.Add(Me.btnLocation)
        Me.Controls.Add(Me.btnCompany)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.AxSB100PC1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "mdiMain"
        Me.Text = "eAlert - Administrator Module "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.AxSB100PC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnCompany As System.Windows.Forms.Button
    Friend WithEvents btnLocation As System.Windows.Forms.Button
    Friend WithEvents btnDepartment As System.Windows.Forms.Button
    Friend WithEvents btnEmpType As System.Windows.Forms.Button
    Friend WithEvents btnShift As System.Windows.Forms.Button
    Friend WithEvents btnEmployee As System.Windows.Forms.Button
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnIPDetails As System.Windows.Forms.Button
    Friend WithEvents btnMailAdd As System.Windows.Forms.Button
    Friend WithEvents btnMailSetup As System.Windows.Forms.Button
    Friend WithEvents btnReports As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnDownClear As System.Windows.Forms.Button
    Friend WithEvents btnSalDisb As System.Windows.Forms.Button
    Friend WithEvents btnDesignation As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents btnManAttn As System.Windows.Forms.Button
    Friend WithEvents btnMachineData As System.Windows.Forms.Button
    Friend WithEvents statusLocation As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents statusUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents statusServerDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents AxSB100PC1 As Axzkemkeeper.AxCZKEM
    Friend WithEvents btnShiftmaster As System.Windows.Forms.Button
    Friend WithEvents btnUserFPDownload As System.Windows.Forms.Button
    Friend WithEvents btnNfh As System.Windows.Forms.Button
    Friend WithEvents btnLogout As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents fg As C1.Win.C1FlexGrid.Classic.C1FlexGridClassic
    Friend WithEvents btnUser As System.Windows.Forms.Button
    Friend WithEvents btnRights As System.Windows.Forms.Button
    Friend WithEvents btnMaster As System.Windows.Forms.Button
    Friend WithEvents btnGraceTime As System.Windows.Forms.Button

End Class

﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Collections.Specialized
#End Region
Public Class frmMailSetup
    Private dp As New ListDictionary
    Dim mStatus As Long
    Private Sub frmMailSetup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Combo_Time()
        cmbTime.SelectedIndex = 0
        Fill_Grid_Details()
    End Sub
    Public Sub Fill_Combo_Time()
        Dim d As DateTime = FormatDateTime("00:00")
        For x As Integer = 0 To 47
            cmbTime.Items.Add(FormatDateTime(d.TimeOfDay.ToString, DateFormat.ShortTime))
            cmbFromTime.Items.Add(FormatDateTime(d.TimeOfDay.ToString, DateFormat.ShortTime))
            cmbToTime.Items.Add(FormatDateTime(d.TimeOfDay.ToString, DateFormat.ShortTime))
            d = d.AddMinutes(60)
        Next
    End Sub
    Public Sub Fill_Grid_Details()
        Dim mStr As String = ""
        With fg
            .Clear()
            .Rows = 10
            .Cols = 3
            .set_ColWidth(0, 150)
            .set_ColWidth(1, 130)
            .set_ColWidth(2, 100)
            .set_TextMatrix(0, 0, "Select")
            .set_TextMatrix(0, 1, "IP Address")
            .set_TextMatrix(0, 2, "IP Mode")
        End With
        SSQL = ""
        SSQL = "Select IPAddress+' -> ' +IPMode from IPAddress_Mst"
        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        For iRow = 0 To mDataSet.Tables(0).Rows.Count - 1
            If iRow = 0 Then
                mStr = mDataSet.Tables(0).Rows(iRow)(0)
                GoTo 1
            End If
            mStr &= "|" & mDataSet.Tables(0).Rows(iRow)(0)
1:
        Next
        fg.ColumnCollection(0).ComboList = mStr
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Fill_Grid_Details()
        cmbTime.SelectedIndex = 0
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub fg_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles fg.AfterEdit
        If e.Col = 0 Then
            Dim iStr() As String
            iStr = Split(fg.get_TextMatrix(fg.RowSel, 0), " -> ")
            fg.set_TextMatrix(fg.RowSel, 1, iStr(0))
            fg.set_TextMatrix(fg.RowSel, 2, iStr(1))
        End If
    End Sub

    Private Sub fg_BeforeEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles fg.BeforeEdit
        If e.Col = 1 Or e.Col = 2 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
1:
        For iRow = 0 To fg.Rows - 1
            If Trim(fg.get_TextMatrix(iRow, 1)) = "" Then
                fg.RemoveItem(iRow)
                GoTo 1
            End If
        Next

        If fg.Rows = 1 Then
            MessageBox.Show("Please Select IP Address Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Fill_Grid_Details()
            Exit Sub
        End If

        SSQL = ""
        SSQL = "Delete from MailSetup_Mst Where Mailtime='" & Trim(cmbTime.Text) & "'"
        mStatus = InsertDeleteUpdate(SSQL)


        For iRow = 1 To fg.Rows - 1
            SSQL = ""
            SSQL = "Insert into MailSetup_Mst Values("
            SSQL = SSQL & "'" & Trim(cmbTime.Text) & "',"
            SSQL = SSQL & "'" & fg.get_TextMatrix(iRow, 1) & "',"
            SSQL = SSQL & "'" & fg.get_TextMatrix(iRow, 2) & "',"
            SSQL = SSQL & "'" & Trim(cmbFromTime.Text) & "',"
            SSQL = SSQL & "'" & Trim(cmbToTime.Text) & "')"
            mStatus = InsertDeleteUpdate(SSQL)
        Next
        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Fill_Grid_Details()
            cmbTime.Focus()
            Exit Sub
        End If
    End Sub

    Private Sub cmbTime_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTime.LostFocus, cmbToTime.LostFocus, cmbFromTime.LostFocus
        Fill_Grid_Details()

        SSQL = ""
        SSQL = "Select IPAddress+' -> ' +IPMode as[ComboText],IPAddress,IPMode,FromTime,ToTime"
        SSQL = SSQL & " from MailSetup_Mst Where Mailtime='" & Trim(cmbTime.Text) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count > 0 Then
            For iRow = 0 To mDataSet.Tables(0).Rows.Count - 1
                fg.set_TextMatrix(iRow + 1, 0, mDataSet.Tables(0).Rows(0)(0))
                fg.set_TextMatrix(iRow + 1, 1, mDataSet.Tables(0).Rows(0)(1))
                fg.set_TextMatrix(iRow + 1, 2, mDataSet.Tables(0).Rows(0)(2))
            Next
            cmbFromTime.SelectedText = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)(3)), "", mDataSet.Tables(0).Rows(0)(3))
            cmbToTime.SelectedText = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)(4)), "", mDataSet.Tables(0).Rows(0)(4))
        End If
    End Sub
End Class
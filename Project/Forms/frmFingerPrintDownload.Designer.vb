﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFingerPrintDownload
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFingerPrintDownload))
        Me.columnHeader9 = New System.Windows.Forms.ColumnHeader
        Me.btnBatchUpdate = New System.Windows.Forms.Button
        Me.btnDownloadTmp9 = New System.Windows.Forms.Button
        Me.columnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader10 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ch8 = New System.Windows.Forms.ColumnHeader
        Me.lvFace = New System.Windows.Forms.ListView
        Me.columnHeader11 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader12 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader13 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader14 = New System.Windows.Forms.ColumnHeader
        Me.tabPage5 = New System.Windows.Forms.TabPage
        Me.groupBox11 = New System.Windows.Forms.GroupBox
        Me.cbUserID3 = New System.Windows.Forms.ComboBox
        Me.label26 = New System.Windows.Forms.Label
        Me.btnGetUserFace = New System.Windows.Forms.Button
        Me.btnDelUserFace = New System.Windows.Forms.Button
        Me.btnDownLoadFace = New System.Windows.Forms.Button
        Me.btnUploadFace = New System.Windows.Forms.Button
        Me.ch7 = New System.Windows.Forms.ColumnHeader
        Me.ch6 = New System.Windows.Forms.ColumnHeader
        Me.ch5 = New System.Windows.Forms.ColumnHeader
        Me.ch3 = New System.Windows.Forms.ColumnHeader
        Me.ch2 = New System.Windows.Forms.ColumnHeader
        Me.ch4 = New System.Windows.Forms.ColumnHeader
        Me.ch1 = New System.Windows.Forms.ColumnHeader
        Me.tabControl2 = New System.Windows.Forms.TabControl
        Me.tabPage4 = New System.Windows.Forms.TabPage
        Me.lvDownload = New System.Windows.Forms.ListView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmbWages = New System.Windows.Forms.ComboBox
        Me.gvsearch = New System.Windows.Forms.DataGridView
        Me.S_No = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ExistingCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OT_Hours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cmbPrefix = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtAttnDate = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.cmbShifDesc = New System.Windows.Forms.ComboBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnClear = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.tabPage5.SuspendLayout()
        Me.groupBox11.SuspendLayout()
        Me.tabControl2.SuspendLayout()
        Me.tabPage4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.gvsearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'columnHeader9
        '
        Me.columnHeader9.Text = "Password"
        '
        'btnBatchUpdate
        '
        Me.btnBatchUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBatchUpdate.Location = New System.Drawing.Point(134, 315)
        Me.btnBatchUpdate.Name = "btnBatchUpdate"
        Me.btnBatchUpdate.Size = New System.Drawing.Size(77, 28)
        Me.btnBatchUpdate.TabIndex = 5
        Me.btnBatchUpdate.Text = "Save"
        Me.btnBatchUpdate.UseVisualStyleBackColor = True
        '
        'btnDownloadTmp9
        '
        Me.btnDownloadTmp9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDownloadTmp9.Location = New System.Drawing.Point(50, 315)
        Me.btnDownloadTmp9.Name = "btnDownloadTmp9"
        Me.btnDownloadTmp9.Size = New System.Drawing.Size(77, 28)
        Me.btnDownloadTmp9.TabIndex = 4
        Me.btnDownloadTmp9.Text = "View"
        Me.btnDownloadTmp9.UseVisualStyleBackColor = True
        '
        'columnHeader8
        '
        Me.columnHeader8.Text = "Name"
        '
        'columnHeader10
        '
        Me.columnHeader10.Text = "Privilege"
        '
        'columnHeader7
        '
        Me.columnHeader7.Text = "UserID"
        '
        'ch8
        '
        Me.ch8.Text = "Flag"
        '
        'lvFace
        '
        Me.lvFace.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnHeader7, Me.columnHeader8, Me.columnHeader9, Me.columnHeader10, Me.columnHeader11, Me.columnHeader12, Me.columnHeader13, Me.columnHeader14})
        Me.lvFace.GridLines = True
        Me.lvFace.Location = New System.Drawing.Point(8, 8)
        Me.lvFace.Name = "lvFace"
        Me.lvFace.Size = New System.Drawing.Size(467, 230)
        Me.lvFace.TabIndex = 68
        Me.lvFace.UseCompatibleStateImageBehavior = False
        Me.lvFace.View = System.Windows.Forms.View.Details
        '
        'columnHeader11
        '
        Me.columnHeader11.Text = "FaceIndex"
        Me.columnHeader11.Width = 42
        '
        'columnHeader12
        '
        Me.columnHeader12.Text = "TmpData"
        '
        'columnHeader13
        '
        Me.columnHeader13.Text = "Length"
        Me.columnHeader13.Width = 40
        '
        'columnHeader14
        '
        Me.columnHeader14.Text = "Enabled"
        '
        'tabPage5
        '
        Me.tabPage5.Controls.Add(Me.lvFace)
        Me.tabPage5.Controls.Add(Me.groupBox11)
        Me.tabPage5.Controls.Add(Me.btnDownLoadFace)
        Me.tabPage5.Controls.Add(Me.btnUploadFace)
        Me.tabPage5.Location = New System.Drawing.Point(4, 22)
        Me.tabPage5.Name = "tabPage5"
        Me.tabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage5.Size = New System.Drawing.Size(482, 343)
        Me.tabPage5.TabIndex = 1
        Me.tabPage5.Text = "Face Tmps"
        Me.tabPage5.UseVisualStyleBackColor = True
        '
        'groupBox11
        '
        Me.groupBox11.Controls.Add(Me.cbUserID3)
        Me.groupBox11.Controls.Add(Me.label26)
        Me.groupBox11.Controls.Add(Me.btnGetUserFace)
        Me.groupBox11.Controls.Add(Me.btnDelUserFace)
        Me.groupBox11.Location = New System.Drawing.Point(8, 281)
        Me.groupBox11.Name = "groupBox11"
        Me.groupBox11.Size = New System.Drawing.Size(467, 58)
        Me.groupBox11.TabIndex = 71
        Me.groupBox11.TabStop = False
        Me.groupBox11.Text = "Get or Delete Binary Face Templates"
        '
        'cbUserID3
        '
        Me.cbUserID3.FormattingEnabled = True
        Me.cbUserID3.Location = New System.Drawing.Point(77, 22)
        Me.cbUserID3.Name = "cbUserID3"
        Me.cbUserID3.Size = New System.Drawing.Size(100, 21)
        Me.cbUserID3.TabIndex = 11
        '
        'label26
        '
        Me.label26.AutoSize = True
        Me.label26.Location = New System.Drawing.Point(30, 27)
        Me.label26.Name = "label26"
        Me.label26.Size = New System.Drawing.Size(40, 13)
        Me.label26.TabIndex = 4
        Me.label26.Text = "UserID"
        '
        'btnGetUserFace
        '
        Me.btnGetUserFace.Location = New System.Drawing.Point(330, 22)
        Me.btnGetUserFace.Name = "btnGetUserFace"
        Me.btnGetUserFace.Size = New System.Drawing.Size(106, 23)
        Me.btnGetUserFace.TabIndex = 3
        Me.btnGetUserFace.Text = "GetUserFace"
        Me.btnGetUserFace.UseVisualStyleBackColor = True
        '
        'btnDelUserFace
        '
        Me.btnDelUserFace.Location = New System.Drawing.Point(201, 22)
        Me.btnDelUserFace.Name = "btnDelUserFace"
        Me.btnDelUserFace.Size = New System.Drawing.Size(97, 23)
        Me.btnDelUserFace.TabIndex = 9
        Me.btnDelUserFace.Text = "DelUserFace"
        Me.btnDelUserFace.UseVisualStyleBackColor = True
        '
        'btnDownLoadFace
        '
        Me.btnDownLoadFace.Location = New System.Drawing.Point(8, 244)
        Me.btnDownLoadFace.Name = "btnDownLoadFace"
        Me.btnDownLoadFace.Size = New System.Drawing.Size(142, 23)
        Me.btnDownLoadFace.TabIndex = 70
        Me.btnDownLoadFace.Text = "DownLoadFaceTemplates"
        Me.btnDownLoadFace.UseVisualStyleBackColor = True
        '
        'btnUploadFace
        '
        Me.btnUploadFace.Location = New System.Drawing.Point(156, 244)
        Me.btnUploadFace.Name = "btnUploadFace"
        Me.btnUploadFace.Size = New System.Drawing.Size(130, 23)
        Me.btnUploadFace.TabIndex = 69
        Me.btnUploadFace.Text = "UploadFaceTemplates"
        Me.btnUploadFace.UseVisualStyleBackColor = True
        '
        'ch7
        '
        Me.ch7.Text = "Ennabled"
        Me.ch7.Width = 81
        '
        'ch6
        '
        Me.ch6.Text = "Password"
        Me.ch6.Width = 40
        '
        'ch5
        '
        Me.ch5.Text = "Privilege"
        Me.ch5.Width = 77
        '
        'ch3
        '
        Me.ch3.Text = "FingerIndex"
        Me.ch3.Width = 52
        '
        'ch2
        '
        Me.ch2.Text = "Name"
        Me.ch2.Width = 41
        '
        'ch4
        '
        Me.ch4.Text = "tmpData"
        Me.ch4.Width = 72
        '
        'ch1
        '
        Me.ch1.Text = "UserID"
        Me.ch1.Width = 54
        '
        'tabControl2
        '
        Me.tabControl2.Controls.Add(Me.tabPage4)
        Me.tabControl2.Controls.Add(Me.tabPage5)
        Me.tabControl2.Enabled = False
        Me.tabControl2.Location = New System.Drawing.Point(506, 4)
        Me.tabControl2.Name = "tabControl2"
        Me.tabControl2.SelectedIndex = 0
        Me.tabControl2.Size = New System.Drawing.Size(490, 369)
        Me.tabControl2.TabIndex = 85
        '
        'tabPage4
        '
        Me.tabPage4.Controls.Add(Me.lvDownload)
        Me.tabPage4.Location = New System.Drawing.Point(4, 22)
        Me.tabPage4.Name = "tabPage4"
        Me.tabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage4.Size = New System.Drawing.Size(482, 343)
        Me.tabPage4.TabIndex = 0
        Me.tabPage4.Text = "Fingerprint Tmps"
        Me.tabPage4.UseVisualStyleBackColor = True
        '
        'lvDownload
        '
        Me.lvDownload.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ch1, Me.ch2, Me.ch3, Me.ch4, Me.ch5, Me.ch6, Me.ch7, Me.ch8})
        Me.lvDownload.GridLines = True
        Me.lvDownload.Location = New System.Drawing.Point(8, 8)
        Me.lvDownload.Name = "lvDownload"
        Me.lvDownload.Size = New System.Drawing.Size(467, 245)
        Me.lvDownload.TabIndex = 0
        Me.lvDownload.UseCompatibleStateImageBehavior = False
        Me.lvDownload.View = System.Windows.Forms.View.Details
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.cmbWages)
        Me.GroupBox1.Controls.Add(Me.gvsearch)
        Me.GroupBox1.Controls.Add(Me.cmbPrefix)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtAttnDate)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmbLocCode)
        Me.GroupBox1.Controls.Add(Me.cmbCompCode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(1, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(502, 308)
        Me.GroupBox1.TabIndex = 89
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(300, 270)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 16)
        Me.Label4.TabIndex = 93
        Me.Label4.Text = "Name"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(358, 267)
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(127, 22)
        Me.txtName.TabIndex = 92
        '
        'cmbWages
        '
        Me.cmbWages.FormattingEnabled = True
        Me.cmbWages.Items.AddRange(New Object() {"BI-MONTHLY- HOSTEL", "MONTHLY-IIND GRADE", "MONTHLY- HOSTEL", "MONTHLY-KARAIKUDI", "BI-MONTHLY-STIPEND", "BI-MONTHLY-MIXING", "MONTHLY-TICKET", "MONTHLY-STAFF"})
        Me.cmbWages.Location = New System.Drawing.Point(228, 86)
        Me.cmbWages.Name = "cmbWages"
        Me.cmbWages.Size = New System.Drawing.Size(121, 21)
        Me.cmbWages.TabIndex = 91
        Me.cmbWages.Visible = False
        '
        'gvsearch
        '
        Me.gvsearch.AllowUserToAddRows = False
        Me.gvsearch.AllowUserToDeleteRows = False
        Me.gvsearch.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.gvsearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvsearch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.S_No, Me.ExistingCode, Me.FirstName, Me.OT_Hours})
        Me.gvsearch.Location = New System.Drawing.Point(15, 110)
        Me.gvsearch.Name = "gvsearch"
        Me.gvsearch.Size = New System.Drawing.Size(469, 151)
        Me.gvsearch.TabIndex = 47
        '
        'S_No
        '
        Me.S_No.HeaderText = "S_No"
        Me.S_No.Name = "S_No"
        '
        'ExistingCode
        '
        Me.ExistingCode.HeaderText = "ExistingCode"
        Me.ExistingCode.Name = "ExistingCode"
        '
        'FirstName
        '
        Me.FirstName.HeaderText = "FirstName"
        Me.FirstName.Name = "FirstName"
        '
        'OT_Hours
        '
        Me.OT_Hours.HeaderText = "OTHours"
        Me.OT_Hours.Name = "OT_Hours"
        '
        'cmbPrefix
        '
        Me.cmbPrefix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrefix.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrefix.FormattingEnabled = True
        Me.cmbPrefix.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"})
        Me.cmbPrefix.Location = New System.Drawing.Point(126, 83)
        Me.cmbPrefix.Name = "cmbPrefix"
        Me.cmbPrefix.Size = New System.Drawing.Size(96, 24)
        Me.cmbPrefix.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(73, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Prefix"
        '
        'txtAttnDate
        '
        Me.txtAttnDate.CustomFormat = "dd/MM/yyyy"
        Me.txtAttnDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAttnDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtAttnDate.Location = New System.Drawing.Point(354, 82)
        Me.txtAttnDate.Name = "txtAttnDate"
        Me.txtAttnDate.Size = New System.Drawing.Size(131, 22)
        Me.txtAttnDate.TabIndex = 44
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(280, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Attn Date"
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(126, 51)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(359, 24)
        Me.cmbLocCode.TabIndex = 1
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(126, 21)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(359, 24)
        Me.cmbCompCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(12, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Location Code"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(12, 25)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(114, 16)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Company Code"
        '
        'cmbShifDesc
        '
        Me.cmbShifDesc.FormattingEnabled = True
        Me.cmbShifDesc.Location = New System.Drawing.Point(405, 320)
        Me.cmbShifDesc.Name = "cmbShifDesc"
        Me.cmbShifDesc.Size = New System.Drawing.Size(121, 21)
        Me.cmbShifDesc.TabIndex = 90
        Me.cmbShifDesc.Visible = False
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btnExit.Location = New System.Drawing.Point(300, 316)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(77, 28)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btnClear.Location = New System.Drawing.Point(217, 315)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(77, 28)
        Me.btnClear.TabIndex = 91
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Location = New System.Drawing.Point(1, 311)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(41, 32)
        Me.PictureBox2.TabIndex = 92
        Me.PictureBox2.TabStop = False
        '
        'frmFingerPrintDownload
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(505, 348)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.cmbShifDesc)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnBatchUpdate)
        Me.Controls.Add(Me.btnDownloadTmp9)
        Me.Controls.Add(Me.tabControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFingerPrintDownload"
        Me.Text = "Manual OT"
        Me.tabPage5.ResumeLayout(False)
        Me.groupBox11.ResumeLayout(False)
        Me.groupBox11.PerformLayout()
        Me.tabControl2.ResumeLayout(False)
        Me.tabPage4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.gvsearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents columnHeader9 As System.Windows.Forms.ColumnHeader
    Private WithEvents btnBatchUpdate As System.Windows.Forms.Button
    Private WithEvents btnDownloadTmp9 As System.Windows.Forms.Button
    Private WithEvents columnHeader8 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader10 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ch8 As System.Windows.Forms.ColumnHeader
    Private WithEvents lvFace As System.Windows.Forms.ListView
    Private WithEvents columnHeader11 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader12 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader13 As System.Windows.Forms.ColumnHeader
    Private WithEvents columnHeader14 As System.Windows.Forms.ColumnHeader
    Private WithEvents tabPage5 As System.Windows.Forms.TabPage
    Private WithEvents groupBox11 As System.Windows.Forms.GroupBox
    Private WithEvents cbUserID3 As System.Windows.Forms.ComboBox
    Private WithEvents label26 As System.Windows.Forms.Label
    Private WithEvents btnGetUserFace As System.Windows.Forms.Button
    Private WithEvents btnDelUserFace As System.Windows.Forms.Button
    Private WithEvents btnDownLoadFace As System.Windows.Forms.Button
    Private WithEvents btnUploadFace As System.Windows.Forms.Button
    Private WithEvents ch7 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch6 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch5 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch3 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch2 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch4 As System.Windows.Forms.ColumnHeader
    Private WithEvents ch1 As System.Windows.Forms.ColumnHeader
    Private WithEvents tabControl2 As System.Windows.Forms.TabControl
    Private WithEvents tabPage4 As System.Windows.Forms.TabPage
    Private WithEvents lvDownload As System.Windows.Forms.ListView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAttnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbShifDesc As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPrefix As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gvsearch As System.Windows.Forms.DataGridView
    Friend WithEvents cmbWages As System.Windows.Forms.ComboBox
    Private WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents S_No As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ExistingCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OT_Hours As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class

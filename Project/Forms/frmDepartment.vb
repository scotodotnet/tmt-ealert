﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmDepartment
    Dim mStatus As Boolean
    Private Sub frmDepartment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Grid_Details()
        mStatus = True
    End Sub
    Public Sub Fill_Grid_Details()
        With c1fgDept
            .Clear()
            .Cols.Fixed = 0
            .Rows.Fixed = 0
            .Cols.Count = 2
            .Rows.Count = 1
            .Cols(0).Width = 100
            .Cols(1).Width = 200
        End With
        mDataSet = New DataSet
        SSQL = ""
        SSQL = "Select deptcode,deptname from Department_Mst Order By deptcode"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                c1fgDept.Rows.Count = .Rows.Count + 1
                For irow = 0 To .Rows.Count - 1
                    c1fgDept(irow, 0) = .Rows(irow)(0)
                    c1fgDept(irow, 1) = .Rows(irow)(1)
                Next
            End If
        End With

    End Sub
    Public Sub Save_Validation()
        mStatus = True
        For irow = 0 To c1fgDept.Rows.Count - 1
            If UCase(Remove_Single_Quote(Trim(txtDepartment.Text))) = Trim(c1fgDept(irow, 0)) Then
                mStatus = False
                MessageBox.Show("Department Already Exists.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save_Validation()
        If mStatus = True Then
            SSQL = ""
            SSQL = "Insert into Department_Mst Values( '" & UCase(Remove_Single_Quote(Trim(txtDepartment.Text))) & "')"
            Dim mSaveStatus As Long
            mSaveStatus = InsertDeleteUpdate(SSQL)
            If mSaveStatus > 0 Then
                txtDepartment.Text = ""
                Fill_Grid_Details()
                txtDepartment.Focus()
                Exit Sub
            End If
        End If
    End Sub
End Class
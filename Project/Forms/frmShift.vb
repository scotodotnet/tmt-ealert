﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmShift
    Dim mStatus As Long
    Private Sub frmShift_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        Fill_Combo_Time()
    End Sub
    Public Sub Fill_Combo_Time()
        Dim d As DateTime = FormatDateTime("00:00")
        For x As Integer = 0 To 47
            cmbStartTime.Items.Add(FormatDateTime(d.TimeOfDay.ToString, DateFormat.ShortTime))
            cmbEndTime.Items.Add(FormatDateTime(d.TimeOfDay.ToString, DateFormat.ShortTime))
            d = d.AddMinutes(30)
        Next
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With

    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Clear_Screen_Details()
    End Sub

    Public Sub Clear_Screen_Details()
        txtShift.Text = ""
        cmbStartTime.SelectedIndex = 0
        cmbEndTime.SelectedIndex = 0
        txtShift.Focus()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please Select Company Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please Select Location Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If

        If Trim(txtShift.Text) = "" Then
            MessageBox.Show("Please Enter shift Description.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtShift.Focus()
            Exit Sub
        End If

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Delete from Shift_Mst where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "' And "
        SSQL = SSQL & "ShiftDesc='" & UCase(Remove_Single_Quote(Trim(txtShift.Text))) & "'"
        mStatus = InsertDeleteUpdate(SSQL)

        SSQL = ""
        SSQL = "Insert into Shift_Mst(CompCode,LocCode,ShiftDesc,StartTime,EndTime) Values ("
        SSQL = SSQL & "'" & iStr1(0) & "',"
        SSQL = SSQL & "'" & iStr2(0) & "',"
        SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtShift.Text))) & "',"
        SSQL = SSQL & "'" & cmbStartTime.Text & "',"
        SSQL = SSQL & "'" & cmbEndTime.Text & "')"

        mStatus = InsertDeleteUpdate(SSQL)
        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Clear_Screen_Details()
            Fill_Grid_Details()
            Exit Sub
        End If
    End Sub

    Private Sub dtpEnd_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Public Sub Fill_Grid_Details()
        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select * from Shift_Mst where CompCode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        fg.Clear()
        fg.Rows.Count = 1
        fg.Cols.Count = 3
        fg.Rows.Fixed = 1
        fg.Cols.Fixed = 0

        fg(0, 0) = "SHIFT"
        fg(0, 1) = "START"
        fg(0, 2) = "END"

        If mDataSet.Tables(0).Rows.Count > 0 Then
            fg.Rows.Count += mDataSet.Tables(0).Rows.Count + 1
            For irow = 0 To mDataSet.Tables(0).Rows.Count - 1
                fg(irow + 1, 0) = mDataSet.Tables(0).Rows(irow)("ShiftDesc")
                fg(irow + 1, 1) = mDataSet.Tables(0).Rows(irow)("StartTime")
                fg(irow + 1, 2) = mDataSet.Tables(0).Rows(irow)("EndTime")
            Next
        End If
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        Fill_Grid_Details()
    End Sub
End Class
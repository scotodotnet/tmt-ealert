﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGraceTime
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox18 = New System.Windows.Forms.GroupBox
        Me.butSave_ShTime = New System.Windows.Forms.Button
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtTime_ShTime = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtMint_ShTime = New System.Windows.Forms.TextBox
        Me.txtTime_TTime = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Grd_ShiftTime = New System.Windows.Forms.DataGridView
        Me.Time = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ToTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Mintus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        CType(Me.Grd_ShiftTime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.Label21)
        Me.GroupBox12.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(681, 43)
        Me.GroupBox12.TabIndex = 75
        Me.GroupBox12.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label21.Location = New System.Drawing.Point(206, 14)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(180, 22)
        Me.Label21.TabIndex = 70
        Me.Label21.Text = "Grace Time Master"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label26.Location = New System.Drawing.Point(20, 305)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(514, 13)
        Me.Label26.TabIndex = 83
        Me.Label26.Text = "TO MAKE CHANGES TO THE GRACE TIME TABLE PLEASE CONTACT YOUR SOFTWARE PROVIDER"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(20, 263)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 22)
        Me.Label2.TabIndex = 82
        Me.Label2.Text = "Notes"
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.butSave_ShTime)
        Me.GroupBox18.Location = New System.Drawing.Point(14, 220)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(323, 46)
        Me.GroupBox18.TabIndex = 81
        Me.GroupBox18.TabStop = False
        '
        'butSave_ShTime
        '
        Me.butSave_ShTime.BackColor = System.Drawing.Color.White
        Me.butSave_ShTime.Enabled = False
        Me.butSave_ShTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave_ShTime.Location = New System.Drawing.Point(139, 10)
        Me.butSave_ShTime.Name = "butSave_ShTime"
        Me.butSave_ShTime.Size = New System.Drawing.Size(75, 30)
        Me.butSave_ShTime.TabIndex = 61
        Me.butSave_ShTime.Text = "&Save"
        Me.butSave_ShTime.UseVisualStyleBackColor = False
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.cmbLocCode)
        Me.GroupBox13.Controls.Add(Me.Label5)
        Me.GroupBox13.Controls.Add(Me.Label4)
        Me.GroupBox13.Controls.Add(Me.Label3)
        Me.GroupBox13.Controls.Add(Me.txtTime_ShTime)
        Me.GroupBox13.Controls.Add(Me.Label11)
        Me.GroupBox13.Controls.Add(Me.Label14)
        Me.GroupBox13.Controls.Add(Me.txtMint_ShTime)
        Me.GroupBox13.Controls.Add(Me.txtTime_TTime)
        Me.GroupBox13.Controls.Add(Me.Label18)
        Me.GroupBox13.Location = New System.Drawing.Point(14, 70)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(323, 154)
        Me.GroupBox13.TabIndex = 80
        Me.GroupBox13.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(140, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 16)
        Me.Label3.TabIndex = 85
        '
        'txtTime_ShTime
        '
        Me.txtTime_ShTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTime_ShTime.Location = New System.Drawing.Point(132, 70)
        Me.txtTime_ShTime.Name = "txtTime_ShTime"
        Me.txtTime_ShTime.Size = New System.Drawing.Size(176, 22)
        Me.txtTime_ShTime.TabIndex = 57
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(37, 73)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 16)
        Me.Label11.TabIndex = 58
        Me.Label11.Text = "STime"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(37, 129)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(52, 16)
        Me.Label14.TabIndex = 63
        Me.Label14.Text = "mintus"
        '
        'txtMint_ShTime
        '
        Me.txtMint_ShTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMint_ShTime.Location = New System.Drawing.Point(132, 126)
        Me.txtMint_ShTime.Name = "txtMint_ShTime"
        Me.txtMint_ShTime.Size = New System.Drawing.Size(176, 22)
        Me.txtMint_ShTime.TabIndex = 64
        '
        'txtTime_TTime
        '
        Me.txtTime_TTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTime_TTime.Location = New System.Drawing.Point(132, 98)
        Me.txtTime_TTime.Name = "txtTime_TTime"
        Me.txtTime_TTime.Size = New System.Drawing.Size(176, 22)
        Me.txtTime_TTime.TabIndex = 66
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(37, 104)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(53, 16)
        Me.Label18.TabIndex = 65
        Me.Label18.Text = "TTime"
        '
        'Grd_ShiftTime
        '
        Me.Grd_ShiftTime.AllowUserToAddRows = False
        Me.Grd_ShiftTime.AllowUserToDeleteRows = False
        Me.Grd_ShiftTime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grd_ShiftTime.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Time, Me.ToTime, Me.Mintus})
        Me.Grd_ShiftTime.Location = New System.Drawing.Point(343, 73)
        Me.Grd_ShiftTime.Name = "Grd_ShiftTime"
        Me.Grd_ShiftTime.ReadOnly = True
        Me.Grd_ShiftTime.Size = New System.Drawing.Size(342, 175)
        Me.Grd_ShiftTime.TabIndex = 79
        '
        'Time
        '
        Me.Time.HeaderText = "Start Minutes"
        Me.Time.Name = "Time"
        Me.Time.ReadOnly = True
        '
        'ToTime
        '
        Me.ToTime.HeaderText = "To Minutes"
        Me.ToTime.Name = "ToTime"
        Me.ToTime.ReadOnly = True
        '
        'Mintus
        '
        Me.Mintus.HeaderText = "Deduct Minutes"
        Me.Mintus.Name = "Mintus"
        Me.Mintus.ReadOnly = True
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(132, 43)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(176, 23)
        Me.cmbLocCode.TabIndex = 86
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(146, 86)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(176, 23)
        Me.cmbCompCode.TabIndex = 85
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(14, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 16)
        Me.Label4.TabIndex = 86
        Me.Label4.Text = "Company Code"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(16, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 16)
        Me.Label5.TabIndex = 87
        Me.Label5.Text = "Location Code"
        '
        'FrmGraceTime
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(698, 340)
        Me.Controls.Add(Me.cmbCompCode)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox18)
        Me.Controls.Add(Me.GroupBox13)
        Me.Controls.Add(Me.Grd_ShiftTime)
        Me.Controls.Add(Me.GroupBox12)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Name = "FrmGraceTime"
        Me.Text = "FrmGraceTime"
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        CType(Me.Grd_ShiftTime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents butSave_ShTime As System.Windows.Forms.Button
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTime_ShTime As System.Windows.Forms.TextBox
    Private WithEvents Label11 As System.Windows.Forms.Label
    Private WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtMint_ShTime As System.Windows.Forms.TextBox
    Friend WithEvents txtTime_TTime As System.Windows.Forms.TextBox
    Private WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Grd_ShiftTime As System.Windows.Forms.DataGridView
    Friend WithEvents Time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mintus As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Private WithEvents Label5 As System.Windows.Forms.Label
    Private WithEvents Label4 As System.Windows.Forms.Label
End Class

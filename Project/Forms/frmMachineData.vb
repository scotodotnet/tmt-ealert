﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Globalization
'Imports Microsoft.Office.Interop.Excel

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms
Imports CrystalDecisions.ReportSource
Imports System.Windows.Forms
Imports System.IO

#End Region

Public Class frmMachineData

    Dim iStr1() As String
    Dim iStr2() As String
    Dim strFilename As String
    Dim xls As Object = CreateObject("Excel.Application")
    Dim xlTitle As String = ""
    Dim strPath As String = Application.StartupPath & "\Excel"

    Private Sub frmMachineData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        dtpFromDate.Value = Now
        dtpToDate.Value = Now
        Fill_Company_Details()
        Fill_shift_Type()
        cmbCompCode.SelectedIndex = 0
    End Sub

    Public Sub Fill_shift_Type()
        SSQL = ""
        SSQL = "Select Distinct ShiftType from Employee_Mst where ShiftType is not null and ShiftType <>'' "

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbShiftType.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbShiftType.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Try
            If Trim(cmbCompCode.Text) = "" Then
                MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbCompCode.Focus()
                Exit Sub
            End If

            If Trim(cmbLocCode.Text) = "" Then
                MessageBox.Show("Please select location code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbLocCode.Focus()
                Exit Sub
            End If

            If Trim(cmbShiftType.Text) = "" Then
                MessageBox.Show("Please select shift type.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbShiftType.Focus()
                Exit Sub
            End If

            iStr1 = Split(cmbCompCode.Text, " | ")
            iStr2 = Split(cmbLocCode.Text, " | ")

            Fill_IN_Time_Details()
            Fill_OUT_Time_Details()
            Fill_Master_Details()


            If xls Is Nothing Then
                MessageBox.Show("Excel is not installed on this machine.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            xls.Application.visible = True
            xls.SheetsInNewWorkbook = 2
            xls.Workbooks.Add()

            xlTitle = Trim(iStr2(0)) & " " & "Machine Datas (IN) " & " From :" & _
                                       dtpFromDate.Value.ToString("dd/MM/yyyy") & " To :" & _
                                       dtpToDate.Value.ToString("dd/MM/yyyy")

            Excel_Preparation(fgIN, 1)

            xlTitle = Trim(iStr2(0)) & " " & "Machine Datas (OUT) " & " From :" & _
                                      dtpFromDate.Value.ToString("dd/MM/yyyy") & " To :" & _
                                      dtpToDate.Value.ToString("dd/MM/yyyy")
            Excel_Preparation(fgOUT, 2)

            If Mid$(strPath, strPath.Length, 1) <> "\" Then
                strPath = strPath & "\"
            End If
            strFilename = strPath & Trim(iStr2(0)) & "_Machine_Datas" & ".xls"
            xls.Application.DisplayAlerts = False
            xls.ActiveCell.Worksheet.SaveAs(strFilename)
            xls.Application.DisplayAlerts = True
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls)
            xls = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Public Sub Excel_Preparation(ByVal fg As Object, ByVal mSheetNo As Integer)


        Try
            With xls
                .Worksheets(mSheetNo).Select()
                .Range("A1:J1").MergeCells = True
                .cells(1, 1).value = xlTitle
                .cells(1, 4).EntireRow.Font.Bold = True


                Dim intI As Integer = 1
                Dim intK As Integer = 1

                For intCol = 0 To fg.Cols - 1
                    .cells(2, intI).value = fg.get_TextMatrix(0, intCol)
                    .cells(2, intI).EntireRow.Font.Bold = True
                    intI += 1
                Next

                intI = 3
                intK = 1

                For intRow = 1 To fg.Rows - 1
                    intK = 1
                    For intCol = 0 To fg.Cols - 1
                        .Cells(intI, intK).value = fg.get_TextMatrix(intRow, intCol)
                        intK += 1
                    Next
                    intI += 1
                Next

            End With

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Fill_Master_Details()
        SSQL = ""
        SSQL = "Select MachineID,ShiftType,DeptName,CatName,subcatName,EmpNo,FirstName,Designation from Employee_Mst"
        SSQL = SSQL & " Where LocCode='" & Trim(iStr2(0)) & "'"
        SSQL = SSQL & " And CompCode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " Order by MachineID"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub

        For iTabRow = 1 To mDataSet.Tables(0).Rows.Count - 1
            For iGrdRow = 1 To fgIN.Rows - 1
                If Trim(mDataSet.Tables(0).Rows(iTabRow - 1)(0)) = Trim(fgIN.get_TextMatrix(iGrdRow, 9)) Then
                    fgIN.set_TextMatrix(iGrdRow, 0, mDataSet.Tables(0).Rows(iTabRow - 1)(1))
                    fgIN.set_TextMatrix(iGrdRow, 1, mDataSet.Tables(0).Rows(iTabRow - 1)(2))
                    fgIN.set_TextMatrix(iGrdRow, 2, mDataSet.Tables(0).Rows(iTabRow - 1)(3))
                    fgIN.set_TextMatrix(iGrdRow, 3, mDataSet.Tables(0).Rows(iTabRow - 1)(4))
                    fgIN.set_TextMatrix(iGrdRow, 4, mDataSet.Tables(0).Rows(iTabRow - 1)(5))
                    fgIN.set_TextMatrix(iGrdRow, 5, mDataSet.Tables(0).Rows(iTabRow - 1)(6))
                    fgIN.set_TextMatrix(iGrdRow, 6, mDataSet.Tables(0).Rows(iTabRow - 1)(7))
                End If
            Next
        Next

        For iTabRow = 1 To mDataSet.Tables(0).Rows.Count - 1
            For iGrdRow = 1 To fgOUT.Rows - 1
                If Trim(mDataSet.Tables(0).Rows(iTabRow - 1)(0)) = Trim(fgOUT.get_TextMatrix(iGrdRow, 9)) Then
                    fgOUT.set_TextMatrix(iGrdRow, 0, mDataSet.Tables(0).Rows(iTabRow - 1)(1))
                    fgOUT.set_TextMatrix(iGrdRow, 1, mDataSet.Tables(0).Rows(iTabRow - 1)(2))
                    fgOUT.set_TextMatrix(iGrdRow, 2, mDataSet.Tables(0).Rows(iTabRow - 1)(3))
                    fgOUT.set_TextMatrix(iGrdRow, 3, mDataSet.Tables(0).Rows(iTabRow - 1)(4))
                    fgOUT.set_TextMatrix(iGrdRow, 4, mDataSet.Tables(0).Rows(iTabRow - 1)(5))
                    fgOUT.set_TextMatrix(iGrdRow, 5, mDataSet.Tables(0).Rows(iTabRow - 1)(6))
                    fgOUT.set_TextMatrix(iGrdRow, 6, mDataSet.Tables(0).Rows(iTabRow - 1)(7))
                End If
            Next
        Next

    End Sub
    Public Sub Fill_IN_Time_Details()
        With fgIN
            .Clear()
            .Rows = 2
            .Cols = 10
            .set_TextMatrix(0, 0, "Shift Type")
            .set_TextMatrix(0, 1, "Department")
            .set_TextMatrix(0, 2, "Category")
            .set_TextMatrix(0, 3, "Sub-Category")
            .set_TextMatrix(0, 4, "Emp No.")
            .set_TextMatrix(0, 5, "Name")
            .set_TextMatrix(0, 6, "Designation")
            .set_TextMatrix(0, 7, "Date")
            .set_TextMatrix(0, 8, "Time IN")
            .set_TextMatrix(0, 9, "Machine ID")
        End With

        SSQL = ""
        SSQL = "select MachineID,RIGHT(CONVERT(VARCHAR,TimeIN, 100),7),convert(varchar(12),TimeIN,104)"
        SSQL = SSQL & " From LogTime_In "
        SSQL = SSQL & " Where LocCode='" & Trim(iStr2(0)) & "'"
        SSQL = SSQL & " And CompCode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " And TimeIN >='" & dtpFromDate.Value.ToString("yyyy/MM/dd HH:MM tt") & "'"
        SSQL = SSQL & " And TimeIN <='" & dtpToDate.Value.ToString("yyyy/MM/dd HH:MM tt") & "'"
        SSQL = SSQL & " Order By TimeIN"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        fgIN.Rows = mDataSet.Tables(0).Rows.Count + 1

        For iRow = 1 To mDataSet.Tables(0).Rows.Count - 1
            fgIN.set_TextMatrix(iRow, 9, Decryption(mDataSet.Tables(0).Rows(iRow - 1)(0)))
            fgIN.set_TextMatrix(iRow, 8, mDataSet.Tables(0).Rows(iRow - 1)(1))
            fgIN.set_TextMatrix(iRow, 7, mDataSet.Tables(0).Rows(iRow - 1)(2))
        Next

    End Sub

    Public Sub Fill_OUT_Time_Details()
        With fgOUT
            .Clear()
            .Rows = 2
            .Cols = 10
            .set_TextMatrix(0, 0, "Shift Type")
            .set_TextMatrix(0, 1, "Department")
            .set_TextMatrix(0, 2, "Category")
            .set_TextMatrix(0, 3, "Sub-Category")
            .set_TextMatrix(0, 4, "Emp No.")
            .set_TextMatrix(0, 5, "Name")
            .set_TextMatrix(0, 6, "Designation")
            .set_TextMatrix(0, 7, "Date")
            .set_TextMatrix(0, 8, "Time OUT")
            .set_TextMatrix(0, 9, "Machine ID")
        End With

        SSQL = ""
        SSQL = "select MachineID,RIGHT(CONVERT(VARCHAR,TimeOUT, 100),7),convert(varchar(12),TimeOUT,104)"
        SSQL = SSQL & " From LogTime_OUT "
        SSQL = SSQL & " Where LocCode='" & Trim(iStr2(0)) & "'"
        SSQL = SSQL & " And CompCode='" & Trim(iStr1(0)) & "'"
        SSQL = SSQL & " And TimeOUT >='" & dtpFromDate.Value.ToString("yyyy/MM/dd HH:MM tt") & "'"
        SSQL = SSQL & " And TimeOUT <='" & dtpToDate.Value.ToString("yyyy/MM/dd HH:MM tt") & "'"
        SSQL = SSQL & " Order By TimeOUT"

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        fgOUT.Rows = mDataSet.Tables(0).Rows.Count + 1

        For iRow = 1 To mDataSet.Tables(0).Rows.Count - 1
            fgOUT.set_TextMatrix(iRow, 9, Decryption(mDataSet.Tables(0).Rows(iRow - 1)(0)))
            fgOUT.set_TextMatrix(iRow, 8, mDataSet.Tables(0).Rows(iRow - 1)(1))
            fgOUT.set_TextMatrix(iRow, 7, mDataSet.Tables(0).Rows(iRow - 1)(2))
        Next

    End Sub
End Class
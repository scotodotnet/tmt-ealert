﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
#End Region
Public Class frmMaster
    Dim mStatus As Long
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Dim mStrComp() As String
    Dim mStrLoc() As String
    Dim val As Integer = 1
    Dim xlTitle As String = ""
    Dim xlFileName As String = ""
    Dim mReportName As String = ""
    Private Sub frmMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        Fill_Location_Details()
        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))

            cmbLocCode.Enabled = True


        Else

        End If

    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Public Sub Fill_Location_Details()
        mStrComp = Split(cmbCompCode.Text, " | ")
        SSQL = ""
        SSQL = "Select LocCode + ' - ' + LocName From Location_Mst "

        mDataSet = Nothing
        mDataSet = ReturnMultipleValue(SSQL)

        cmbLocCode.Items.Clear()

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
        Dim iRow As Integer = 0

        Do
            cmbLocCode.Items.Add(Trim(mDataSet.Tables(0).Rows(iRow)(0)))
            cmbLocCode.SelectedIndex = 0
            iRow += 1
            While iRow > mDataSet.Tables(0).Rows.Count - 1 : Exit Sub : End While

        Loop
        'cmbLocCode.Text = ""
    End Sub

  
    Private Sub btnweekofDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnweekofDown.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Please select company code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Please select LCode code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        iStr1 = Split(cmbCompCode.Text, " | ")
        'iStr2 = Split(cmbLocCode.Text, " | ")

        mReportName = "WEEK-OFF-DOWNLOAD"
        Fill_Weekof_details()
        If fg.Rows < 2 Then Exit Sub

        Dim xls As Object = CreateObject("Excel.Application")

        Dim strFilename As String
        Dim intCol, intRow As Integer


        Dim strPath As String = Application.StartupPath & "\Excel"

        If xls Is Nothing Then
            MessageBox.Show("Excel is not installed on this machine.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        With xls


            .application.visible = True
            .SheetsInNewWorkbook = 3
            .Workbooks.Add()
            .Worksheets(1).Select()


            Dim intI As Integer = 1
            Dim intK As Integer = 1
            If mReportName <> "TOTAL DAYS ATTENDANCE - BETWEEN DATES" Then
                If mReportName = "DAY ATTENDANCE - SHIFT WISE" Then
                    For intCol = 0 To xlgrid.Cols - 1
                        .cells(1, intI).value = xlgrid.get_TextMatrix(0, intCol)
                        .cells(1, intI).EntireRow.Font.Bold = True
                        intI += 1
                    Next

                    intI = 3
                    intK = 1

                    For intRow = 1 To xlgrid.Rows - 1
                        intK = 1
                        For intCol = 0 To xlgrid.Cols - 1
                            .Cells(intI, intK).value = xlgrid.get_TextMatrix(intRow, intCol)
                            intK += 1
                        Next
                        intI += 1
                    Next


                Else
                    For intCol = 0 To fg.Cols - 1
                        .cells(1, intI).value = fg.get_TextMatrix(0, intCol)
                        .cells(1, intI).EntireRow.Font.Bold = True
                        intI += 1
                    Next
                    .cells(1, intI).Value = "WeekOff"

                    'Dim dayCount As Integer = DateDiff(DateInterval.Day, dtpFromShift.Value.Date, dtpToDateShift.Value.Date)
                    'Dim daysAdded As Integer = 0
                    'While dayCount >= 0
                    '    .cells(1, intI).Value = Format(dtpFromShift.Value.Date.AddDays(daysAdded), "dd/MM/yyyy")

                    '    intI += 1
                    '    dayCount -= 1
                    '    daysAdded += 1
                    'End While

                    intI = 2
                    intK = 1

                    For intRow = 1 To fg.Rows - 1
                        intK = 1
                        For intCol = 0 To fg.Cols - 1
                            .Cells(intI, intK).value = fg.get_TextMatrix(intRow, intCol)
                            intK += 1
                        Next
                        intI += 1
                    Next
                End If

            End If

            If Mid$(strPath, strPath.Length, 1) <> "\" Then
                strPath = strPath & "\"
            End If
            strFilename = strPath & xlFileName & "UNIT I-ALL.xls"
            .Application.DisplayAlerts = False
            .ActiveCell.Worksheet.SaveAs(strFilename)
            .Application.DisplayAlerts = True
        End With
        System.Runtime.InteropServices.Marshal.ReleaseComObject(xls)
        xls = Nothing
        MsgBox("Report Export Sucessfully")

    End Sub
    Public Sub Fill_Weekof_details()
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        Dim Fromdate As String = dtpFromWeek.Text
        'Dim ToDate As String = dtpToWeek.Text
        Dim year() As String
        year = Split(Trim(dtpFromWeek.Text), "/")
        Dim valmonth As String = year(1)
        Dim Months As String = ""
        Dim FinalYear As Integer = year(2)


        If valmonth = "01" Then
            Months = "January"
        ElseIf valmonth = "02" Then
            Months = "February"
        ElseIf valmonth = "03" Then
            Months = "March"
        ElseIf valmonth = "04" Then
            Months = "April"
        ElseIf valmonth = "05" Then
            Months = "May"
        ElseIf valmonth = "06" Then
            Months = "June"
        ElseIf valmonth = "07" Then
            Months = "July"
        ElseIf valmonth = "08" Then
            Months = "August"
        ElseIf valmonth = "09" Then
            Months = "September"
        ElseIf valmonth = "10" Then
            Months = "October"
        ElseIf valmonth = "11" Then
            Months = "November"
        ElseIf valmonth = "12" Then
            Months = "December"
        End If



        If mReportName = "WEEK-OFF-DOWNLOAD" Then
            SSQL = ""
            SSQL = "select MachineID,isnull(CatName,'') as Catgory,isnull(DeptName,'') as [DeptName]"
            'SSQL = SSQL & ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]"
            SSQL = SSQL & ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]"
            'SSQL = SSQL & ",isnull(SubCatName,'') as [SubCatName]"
            SSQL = SSQL & " from Employee_Mst Where Compcode='" & iStr1(0) & "' and wages='" & cmbWages.Text & "'"
            SSQL = SSQL & " And  IsActive='Yes' "

            mDataSet = ReturnMultipleValue(SSQL)
            With fg
                .Rows = 1
                .Cols = 6
                .set_TextMatrix(0, 0, "MachineID")
                .set_TextMatrix(0, 1, "Catgory")
                .set_TextMatrix(0, 2, "DeptName")
                .set_TextMatrix(0, 3, "FirstName")
                .set_TextMatrix(0, 4, "Months")
                .set_TextMatrix(0, 5, "Year")
            End With

            If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub
            fg.Rows = fg.Rows + mDataSet.Tables(0).Rows.Count + 1

            For iRow = 0 To mDataSet.Tables(0).Rows.Count - 1
                fg.set_TextMatrix(iRow + 1, 0, mDataSet.Tables(0).Rows(iRow)(0))
                fg.set_TextMatrix(iRow + 1, 1, mDataSet.Tables(0).Rows(iRow)(1))
                fg.set_TextMatrix(iRow + 1, 2, mDataSet.Tables(0).Rows(iRow)(2))
                fg.set_TextMatrix(iRow + 1, 3, mDataSet.Tables(0).Rows(iRow)(3))
                fg.set_TextMatrix(iRow + 1, 4, Months)
                fg.set_TextMatrix(iRow + 1, 5, FinalYear)

            Next
        End If
        Exit Sub



    End Sub


    Private Sub btnweekofupload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnweekofupload.Click
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim intRow As Integer
            Dim SSQL As String = ""
            Dim intCol As Integer = 0
            Dim dsEmployee As DataSet
            Dim Errflag As Boolean = False
            Dim Errflag_30 As Boolean = False
            Dim Errflag_31 As Boolean = False
            Dim Errflag_29 As Boolean = False
            Dim Effflag_28 As Boolean = False
            Dim month As String = ""
            Dim intI As Integer = 1
            Dim intK As Integer = 1
            Dim valMachineID As String = ""
            Dim valCatgory As String = ""
            Dim valDeptName As String = ""
            Dim valFirstName As String = ""
            Dim Months As String = ""
            Dim FYear As String = ""
            Dim ValWeekoff As String = ""
            Dim ErrFlag_Weekof As Boolean = False



            ShitupGrd.ClearSelection()

            Dim ff As String = dtpFromWeek.Value.Month

            Dim SSQL1 As String = ""
            Dim SSSQL As String = ""



            If Errflag = False Then




                Dim xls As Object = CreateObject("Excel.Application")
                With xls

                    Dim fullpath As String = OpenFileDialog1.FileName
                    Dim MyConnection As System.Data.OleDb.OleDbConnection
                    Dim DtSet As System.Data.DataSet

                    'Dim dayCount As Integer = DateDiff(DateInterval.Day, dtpFromWeek.Value.Date, dtpToWeek.Value.Date)
                    'dayCount = dayCount + 1

                    'If dayCount = "31" Or dayCount = "30" Or dayCount = "29" Or dayCount = "28" Then


                    'Else
                    '    Errflag = True
                    '    MessageBox.Show("Plz Upload Only Month Upload.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    '    Exit Sub

                    'End If

                    Dim iStr() As String
                    iStr = Split(Trim(cmbCompCode.Text), " | ")

                    Dim val As System.Data.DataTable

                    Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
                    MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fullpath + ";Extended Properties=Excel 8.0;")
                    MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
                    MyCommand.TableMappings.Add("Table", "Net-informations.com")
                    DtSet = New System.Data.DataSet
                    val = New System.Data.DataTable

                    DtSet.Tables.Add(val)
                    ShitupGrd.DataSource = val(0)
                    MyCommand.Fill(val)

                    For m = 0 To DtSet.Tables(0).Rows.Count - 1
                        If DtSet.Tables(0).Rows(m)("Weekoff").ToString() = "" Then
                            MessageBox.Show("ROW EMPTY", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Exit Sub
                        Else
                            'skip
                        End If


                    Next

                    Dim Fromdate As String = dtpFromWeek.Text
                    Dim year() As String
                    year = Split(Trim(dtpFromWeek.Text), "/")
                    Dim valmonth As String = year(1)


                    If valmonth = "01" Then
                        month = "January"
                    ElseIf valmonth = "02" Then
                        month = "February"
                    ElseIf valmonth = "03" Then
                        month = "March"
                    ElseIf valmonth = "04" Then
                        month = "April"
                    ElseIf valmonth = "05" Then
                        month = "May"
                    ElseIf valmonth = "06" Then
                        month = "June"
                    ElseIf valmonth = "07" Then
                        month = "July"
                    ElseIf valmonth = "08" Then
                        month = "August"
                    ElseIf valmonth = "09" Then
                        month = "September"
                    ElseIf valmonth = "10" Then
                        month = "October"
                    ElseIf valmonth = "11" Then
                        month = "November"
                    ElseIf valmonth = "12" Then
                        month = "December"
                    End If

                    'SSQL1 = "select Year,Months from MstShifUpload where Months='" & month & "' and Year='" & year(2) & "' and  EmpCatgory='" & cmbEmp_Cat_head.Text & "'"
                    'dsEmployee = ReturnMultipleValue(SSQL1)
                    'If dsEmployee.Tables(0).Rows.Count = 0 Then
                    'Else
                    '    Errflag = True
                    '    MessageBox.Show("Already Uploaded This Month.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    '    Exit Sub

                    'End If

                    Dim sql As String = ""
                    Dim Sts As String = ""
                    For i = 0 To DtSet.Tables(0).Rows.Count - 1

                        valMachineID = val.Rows(i)(0)
                        valCatgory = val.Rows(i)(1)
                        valDeptName = val.Rows(i)(2)
                        valFirstName = val.Rows(i)(3)
                        Months = val.Rows(i)(4)
                        FYear = val.Rows(i)(5)
                        ValWeekoff = val.Rows(i)(6)
                        ErrFlag_Weekof = True
                        If (ErrFlag_Weekof = True) Then
                            Dim DaSet1 As New DataSet
                            Dim SSQLL As String = ""
                            Dim Val_week As String = ""
                            Dim Query As String = ""
                            SSQLL = "select * from MstweekoffUpdate where MachineID='" & valMachineID & "' and Months ='" & Months & "' and Year='" & FYear & "'"
                            DaSet1 = ReturnMultipleValue(SSQLL)
                            If DaSet1.Tables(0).Rows.Count > 0 Then
                                Query = "update MstweekoffUpdate set Weekoff='" & ValWeekoff & "',Date='" & Fromdate & "'  where Months='" & Months & "' and Year='" & FYear & "'"
                                mStatus = InsertDeleteUpdate(Query)
                            Else
                                Query = "insert into MstweekoffUpdate(CompCode,MachineID,EmpName,CatName,DeptName,Date,Months,Year,Weekoff) "
                                Query = Query & " values('" & iStr(0) & "','" & valMachineID & "',"
                                Query = Query & " '" & valFirstName & "','" & valCatgory & "','" & valDeptName & "','" & Fromdate & "','" & Months & "','" & FYear & "','" & ValWeekoff & "')"
                                mStatus = InsertDeleteUpdate(Query)

                            End If

                        End If

                    Next




                    MessageBox.Show("Successfully Insert.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    MyConnection.Close()
                End With
            End If
        End If

    End Sub
End Class
﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
#End Region
Public Class frmMailAdd
    Dim mStatus As Long
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtFrom.Text = ""
        txtPassword.Text = ""
        txtTo.Text = ""
        txtCC.Text = ""
        txtBCC.Text = ""
        txtSubject.Text = ""
        cmbCompCode.Focus()
    End Sub

    Private Sub frmMailAdd_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Trim(cmbCompCode.Text) = "" Then
            MessageBox.Show("Plase select company.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbCompCode.Focus()
            Exit Sub
        End If

        If Trim(cmbLocCode.Text) = "" Then
            MessageBox.Show("Plase select loaction.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmbLocCode.Focus()
            Exit Sub
        End If
        If Trim(txtFrom.Text) = "" Then
            MessageBox.Show("Please Enter From Mail Address Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFrom.Focus()
            Exit Sub
        End If

        If Trim(txtPassword.Text) = "" Then
            MessageBox.Show("Please Enter Password.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtPassword.Focus()
            Exit Sub
        End If

        If Trim(txtTo.Text) = "" Then
            MessageBox.Show("Please Enter To Mail Address Details.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtTo.Focus()
            Exit Sub
        End If

        If MailIDChecking(txtFrom.Text) = False Then
            MessageBox.Show("Please Enter Proper Mail ID.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFrom.Focus()
            Exit Sub
        End If

        If MailIDChecking(txtTo.Text) = False Then
            MessageBox.Show("Please Enter Proper Mail ID.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtTo.Focus()
            Exit Sub
        End If


        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        SSQL = ""
        SSQL = "Delete from MailAdd_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"

        mStatus = InsertDeleteUpdate(SSQL)

        SSQL = ""
        SSQL = "Insert into MailAdd_Mst Values("
        SSQL = SSQL & "'" & iStr1(0) & "',"
        SSQL = SSQL & "'" & iStr2(0) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtFrom.Text)) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtPassword.Text)) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtTo.Text)) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtCC.Text)) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtBCC.Text)) & "',"
        SSQL = SSQL & "'" & Trim(Remove_Single_Quote(txtSubject.Text)) & "')"
        mStatus = InsertDeleteUpdate(SSQL)
        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFrom.Text = ""
            txtPassword.Text = ""
            txtTo.Text = ""
            txtCC.Text = ""
            txtBCC.Text = ""
            txtSubject.Text = ""
            cmbCompCode.Focus()
            Exit Sub
        End If
    End Sub
    Public Function MailIDChecking(ByVal mvarMailD As String) As Boolean
        Dim mvarMailID() As String
        Dim MailID As String

        mvarMailID = Split(mvarMailD, ",")

        For Each MailID In mvarMailID
            If EmailAddressCheck(MailID) = False Then
                Return False
                Exit Function
            End If
        Next
        Return True
    End Function
    Public Sub Fill_Screen_Details()
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        SSQL = ""
        SSQL = "Select * from MailAdd_Mst Where CompCode='" & iStr1(0) & "'"
        SSQL = SSQL & " And LocCode='" & iStr2(0) & "'"
        mDataSet = ReturnMultipleValue(SSQL)

        If mDataSet.Tables(0).Rows.Count <= 0 Then Exit Sub

        With mDataSet.Tables(0)
            txtFrom.Text = .Rows(0)("FromAdd")
            txtPassword.Text = .Rows(0)("FromPWD")
            txtTo.Text = .Rows(0)("ToAdd")
            txtCC.Text = .Rows(0)("CCAdd")
            txtBCC.Text = .Rows(0)("BCCAdd")
            txtSubject.Text = .Rows(0)("Subject")
            txtFrom.Focus()
            Exit Sub
        End With
    End Sub

    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLocCode.LostFocus
        Fill_Screen_Details()
    End Sub
End Class
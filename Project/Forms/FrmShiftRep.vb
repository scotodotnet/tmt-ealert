﻿Public Class FrmShiftRep

    Private Sub label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label6.Click

    End Sub

    Private Sub FrmShiftRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        'Employee_Load()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name],CompCode from Company_Mst Order By CompName"
        cmbCompCode.Items.Clear()
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.DataSource = mDataSet.Tables(0)
                cmbCompCode.DisplayMember = "[Name]"
                cmbCompCode.ValueMember = "CompCode"
                'cmbCompCode.Items.Clear()
                'For iRow = 0 To .Rows.Count - 1
                '    cmbCompCode.Items.Add(.Rows(iRow)(0))
                'Next
            End If
        End With
    End Sub
    Public Sub Employee_Load()

        cmbMacnoReg.Items.Clear()
        If cmbCompCode.SelectedValue = "" Then
            MessageBox.Show("Select the Company Code", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        ElseIf cmbLocCode.SelectedValue = "" Then
            MessageBox.Show("Select the Location Code", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            SSQL = ""
            SSQL = "Select (MachineID + '->' + EmpNo + '->' + FirstName) as FirstName,MachineID from Employee_Mst where IsActive='Yes' and CompCode='" & cmbCompCode.SelectedValue & "' and LocCode='" & cmbLocCode.SelectedValue & "' order by convert(numeric,MachineID) asc"
            mDataSet = ReturnMultipleValue(SSQL)
            If mDataSet.Tables(0).Rows.Count > 0 Then
                cmbMacnoReg.DataSource = mDataSet.Tables(0)
                cmbMacnoReg.ValueMember = "MachineID"
                cmbMacnoReg.DisplayMember = "FirstName"
            End If
        End If
        
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        gvReport.Rows.Clear()
        Dim Datevalue As String = DateTime.Today.ToShortDateString()
        If cmbMacnoReg.SelectedValue = "" Then
            MessageBox.Show("Select the Employee No", "Altius InfoSystems", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            SSQL = ""
            SSQL = "Select Category from Shiftrrecording where MachineNo='" & cmbMacnoReg.SelectedValue & "' and Ccode='" & cmbCompCode.SelectedValue & "' and Lcode='" & cmbLocCode.SelectedValue & "'"
            Dim str As String = ReturnSingleValue(SSQL)
            If str = "P" Then
                Dim qry As String = "Select SR.ShiftName,SR.WeekOff,SD.ShiftDetails from Shiftrrecording SR inner Join Shiftdetails SD on SD.ShiftCode=SR.ShiftName where SR.MachineNo='" & cmbMacnoReg.SelectedValue & "' and SR.Ccode='" & cmbCompCode.SelectedValue & "' and SR.Lcode='" & cmbLocCode.SelectedValue & "'"
                mDataSet = ReturnMultipleValue(qry)
                If mDataSet.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To 10
                        Datevalue = (Convert.ToDateTime(Datevalue).AddDays(1)).ToShortDateString()
                        Dim n As Integer = gvReport.Rows.Add()
                        gvReport.Rows(n).Cells(0).Value = n + 1
                        gvReport.Rows(n).Cells(1).Value = Datevalue
                        Dim week As String = Convert.ToDateTime(Datevalue).DayOfWeek.ToString()
                        Dim QRY1 As String = "  Select CONVERT(varchar, NFHDate,103) AS NFHDate from NFH_Mst where CONVERT(varchar, NFHDate, 103)='" & Datevalue & "'"
                        Dim nfH As String = ReturnSingleValue(QRY1)
                        If Trim(nfH) = Datevalue Then
                            gvReport.Rows(n).Cells(2).Value = "NH"
                        Else
                            If Trim(week.ToString()).ToLower() = Trim(mDataSet.Tables(0).Rows(0)("WeekOff").ToString()).ToLower() Then
                                gvReport.Rows(n).Cells(2).Value = "WH"
                            Else
                                gvReport.Rows(n).Cells(2).Value = mDataSet.Tables(0).Rows(0)("ShiftDetails").ToString()
                            End If
                        End If


                        

                    Next

                End If
            Else
                Dim qry As String = "Select SR.ShiftName,SR.WeekOff,SD.ShiftDetails,SR.AutoRotateShift,SR.NextShift,SR.NextDate,SR.NoDays from Shiftrrecording SR inner Join Shiftdetails SD on SD.ShiftCode=SR.ShiftName or SD.ShiftCode=SR.NextShift where SR.MachineNo='" & cmbMacnoReg.SelectedValue & "' and SR.Ccode='" & cmbCompCode.SelectedValue & "' and SR.Lcode='" & cmbLocCode.SelectedValue & "'"
                mDataSet = ReturnMultipleValue(qry)
                If mDataSet.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To Convert.ToInt32(mDataSet.Tables(0).Rows(0)("NoDays").ToString())
                        Datevalue = (Convert.ToDateTime(Datevalue).AddDays(1)).ToShortDateString()
                        Dim n As Integer = gvReport.Rows.Add()
                        gvReport.Rows(n).Cells(0).Value = n + 1
                        gvReport.Rows(n).Cells(1).Value = Datevalue
                        Dim week As String = Convert.ToDateTime(Datevalue).DayOfWeek.ToString()
                        If Trim(week.ToString()).ToLower() = Trim(mDataSet.Tables(0).Rows(0)("WeekOff").ToString()).ToLower() Then
                            gvReport.Rows(n).Cells(2).Value = "WH"
                        Else
                            If Convert.ToDateTime(Datevalue) = Convert.ToDateTime(mDataSet.Tables(0).Rows(0)("NextDate").ToString()) Then
                                gvReport.Rows(n).Cells(2).Value = mDataSet.Tables(0).Rows(0)("ShiftDetails").ToString()
                            Else
                                gvReport.Rows(n).Cells(2).Value = mDataSet.Tables(0).Rows(0)("ShiftDetails").ToString()
                            End If
                        End If

                    Next

                End If

            End If
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged
        
    End Sub

    Private Sub cmbCompCode_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbCompCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Trim(cmbCompCode.Text) = "" Then Exit Sub
            Dim iStr() As String
            iStr = Split(Trim(cmbCompCode.Text), " | ")

            SSQL = ""
            SSQL = "Select LocCode + ' | ' + LocName as [Name],LocCode from Location_Mst Where Compcode='"
            SSQL = SSQL & "" & iStr(0) & "'"

            mDataSet = ReturnMultipleValue(SSQL)

            With mDataSet.Tables(0)
                cmbLocCode.DataSource = mDataSet.Tables(0)
                cmbLocCode.DisplayMember = "[Name]"
                cmbLocCode.ValueMember = "LocCode"
                If .Rows.Count > 0 Then
                    'cmbLocCode.Items.Clear()
                    'For iRow = 0 To .Rows.Count - 1
                    '    cmbLocCode.Items.Add(.Rows(iRow)(0))
                    'Next
                End If
            End With
        End If
    End Sub

    Private Sub cmbLocCode_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbLocCode.KeyDown
        If e.KeyCode = Keys.Enter Then
            Employee_Load()
        End If
    End Sub

    Private Sub cmbMacnoReg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMacnoReg.SelectedIndexChanged
        Dim dtEmpty As New DataTable()
        'gvReport.DataSource = dtEmpty

    End Sub
End Class
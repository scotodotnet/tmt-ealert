﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShiftRecording
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnclr = New System.Windows.Forms.Button
        Me.Cancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.panel1 = New System.Windows.Forms.Panel
        Me.txtStartDay = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtstartreg = New System.Windows.Forms.TextBox
        Me.label19 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.label20 = New System.Windows.Forms.Label
        Me.lblCurrentDate = New System.Windows.Forms.Label
        Me.dataGridView1 = New System.Windows.Forms.DataGridView
        Me.btnForward = New System.Windows.Forms.Button
        Me.label21 = New System.Windows.Forms.Label
        Me.cmbWeekReg = New System.Windows.Forms.ComboBox
        Me.btnprevious = New System.Windows.Forms.Button
        Me.label17 = New System.Windows.Forms.Label
        Me.label22 = New System.Windows.Forms.Label
        Me.label23 = New System.Windows.Forms.Label
        Me.label24 = New System.Windows.Forms.Label
        Me.label25 = New System.Windows.Forms.Label
        Me.tabControl1 = New System.Windows.Forms.TabControl
        Me.tabReg = New System.Windows.Forms.TabPage
        Me.cmbshift3 = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmbshift2 = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cmbShift1 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btndays = New System.Windows.Forms.Button
        Me.checkBox1 = New System.Windows.Forms.CheckBox
        Me.txtDaysreg = New System.Windows.Forms.TextBox
        Me.label18 = New System.Windows.Forms.Label
        Me.CmbShiftTypeReg = New System.Windows.Forms.ComboBox
        Me.label16 = New System.Windows.Forms.Label
        Me.cmbShiftnamereg = New System.Windows.Forms.ComboBox
        Me.label9 = New System.Windows.Forms.Label
        Me.txtNamereg = New System.Windows.Forms.TextBox
        Me.label8 = New System.Windows.Forms.Label
        Me.cmbMacnoReg = New System.Windows.Forms.ComboBox
        Me.label6 = New System.Windows.Forms.Label
        Me.tabPer = New System.Windows.Forms.TabPage
        Me.txtEmpNameper = New System.Windows.Forms.TextBox
        Me.cmbShiftNamePer = New System.Windows.Forms.ComboBox
        Me.label5 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.cmbMacNoPer = New System.Windows.Forms.ComboBox
        Me.label3 = New System.Windows.Forms.Label
        Me.btnReport = New System.Windows.Forms.Button
        Me.cmbCompCode = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.cmbLocCode = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnHoliday = New System.Windows.Forms.Button
        Me.panel1.SuspendLayout()
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl1.SuspendLayout()
        Me.tabReg.SuspendLayout()
        Me.tabPer.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnclr
        '
        Me.btnclr.BackColor = System.Drawing.Color.White
        Me.btnclr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclr.Location = New System.Drawing.Point(399, 355)
        Me.btnclr.Name = "btnclr"
        Me.btnclr.Size = New System.Drawing.Size(75, 28)
        Me.btnclr.TabIndex = 27
        Me.btnclr.Text = "Clear"
        Me.btnclr.UseVisualStyleBackColor = False
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.White
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(480, 355)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(75, 28)
        Me.Cancel.TabIndex = 26
        Me.Cancel.Text = "Cancel"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(318, 355)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 28)
        Me.btnSave.TabIndex = 25
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Location = New System.Drawing.Point(237, 355)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 28)
        Me.btnEdit.TabIndex = 24
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'panel1
        '
        Me.panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.panel1.Controls.Add(Me.txtStartDay)
        Me.panel1.Controls.Add(Me.Label11)
        Me.panel1.Controls.Add(Me.txtstartreg)
        Me.panel1.Controls.Add(Me.label19)
        Me.panel1.Controls.Add(Me.label1)
        Me.panel1.Controls.Add(Me.label20)
        Me.panel1.Controls.Add(Me.lblCurrentDate)
        Me.panel1.Controls.Add(Me.dataGridView1)
        Me.panel1.Controls.Add(Me.btnForward)
        Me.panel1.Controls.Add(Me.label21)
        Me.panel1.Controls.Add(Me.cmbWeekReg)
        Me.panel1.Controls.Add(Me.btnprevious)
        Me.panel1.Controls.Add(Me.label17)
        Me.panel1.Controls.Add(Me.label22)
        Me.panel1.Controls.Add(Me.label23)
        Me.panel1.Controls.Add(Me.label24)
        Me.panel1.Controls.Add(Me.label25)
        Me.panel1.Location = New System.Drawing.Point(428, 66)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(363, 271)
        Me.panel1.TabIndex = 23
        '
        'txtStartDay
        '
        Me.txtStartDay.BackColor = System.Drawing.Color.White
        Me.txtStartDay.Enabled = False
        Me.txtStartDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDay.ForeColor = System.Drawing.Color.Black
        Me.txtStartDay.Location = New System.Drawing.Point(139, 225)
        Me.txtStartDay.Name = "txtStartDay"
        Me.txtStartDay.Size = New System.Drawing.Size(183, 24)
        Me.txtStartDay.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(9, 229)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 16)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Start Day"
        '
        'txtstartreg
        '
        Me.txtstartreg.BackColor = System.Drawing.Color.White
        Me.txtstartreg.Enabled = False
        Me.txtstartreg.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstartreg.ForeColor = System.Drawing.Color.Black
        Me.txtstartreg.Location = New System.Drawing.Point(139, 198)
        Me.txtstartreg.Name = "txtstartreg"
        Me.txtstartreg.Size = New System.Drawing.Size(183, 24)
        Me.txtstartreg.TabIndex = 16
        '
        'label19
        '
        Me.label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label19.ForeColor = System.Drawing.Color.Black
        Me.label19.Location = New System.Drawing.Point(3, 0)
        Me.label19.Name = "label19"
        Me.label19.Size = New System.Drawing.Size(50, 19)
        Me.label19.TabIndex = 7
        Me.label19.Text = "Sun"
        Me.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.ForeColor = System.Drawing.Color.White
        Me.label1.Location = New System.Drawing.Point(9, 200)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(77, 16)
        Me.label1.TabIndex = 15
        Me.label1.Text = "Start Date"
        '
        'label20
        '
        Me.label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label20.ForeColor = System.Drawing.Color.Black
        Me.label20.Location = New System.Drawing.Point(309, 0)
        Me.label20.Name = "label20"
        Me.label20.Size = New System.Drawing.Size(50, 19)
        Me.label20.TabIndex = 13
        Me.label20.Text = "Sat"
        Me.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentDate
        '
        Me.lblCurrentDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentDate.ForeColor = System.Drawing.Color.White
        Me.lblCurrentDate.Location = New System.Drawing.Point(84, 148)
        Me.lblCurrentDate.Name = "lblCurrentDate"
        Me.lblCurrentDate.Size = New System.Drawing.Size(195, 23)
        Me.lblCurrentDate.TabIndex = 18
        Me.lblCurrentDate.Text = "label1"
        Me.lblCurrentDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dataGridView1
        '
        Me.dataGridView1.AllowUserToAddRows = False
        Me.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dataGridView1.ColumnHeadersVisible = False
        Me.dataGridView1.EnableHeadersVisualStyles = False
        Me.dataGridView1.Location = New System.Drawing.Point(3, 20)
        Me.dataGridView1.Name = "dataGridView1"
        Me.dataGridView1.ReadOnly = True
        Me.dataGridView1.RowHeadersVisible = False
        Me.dataGridView1.Size = New System.Drawing.Size(358, 123)
        Me.dataGridView1.TabIndex = 0
        '
        'btnForward
        '
        Me.btnForward.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnForward.Location = New System.Drawing.Point(285, 147)
        Me.btnForward.Name = "btnForward"
        Me.btnForward.Size = New System.Drawing.Size(75, 23)
        Me.btnForward.TabIndex = 17
        Me.btnForward.Text = ">>"
        Me.btnForward.UseVisualStyleBackColor = True
        '
        'label21
        '
        Me.label21.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label21.ForeColor = System.Drawing.Color.Black
        Me.label21.Location = New System.Drawing.Point(258, 0)
        Me.label21.Name = "label21"
        Me.label21.Size = New System.Drawing.Size(50, 19)
        Me.label21.TabIndex = 12
        Me.label21.Text = "Fri"
        Me.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbWeekReg
        '
        Me.cmbWeekReg.BackColor = System.Drawing.Color.White
        Me.cmbWeekReg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWeekReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbWeekReg.ForeColor = System.Drawing.Color.Black
        Me.cmbWeekReg.FormattingEnabled = True
        Me.cmbWeekReg.Location = New System.Drawing.Point(139, 171)
        Me.cmbWeekReg.Name = "cmbWeekReg"
        Me.cmbWeekReg.Size = New System.Drawing.Size(183, 24)
        Me.cmbWeekReg.TabIndex = 10
        '
        'btnprevious
        '
        Me.btnprevious.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnprevious.Location = New System.Drawing.Point(3, 148)
        Me.btnprevious.Name = "btnprevious"
        Me.btnprevious.Size = New System.Drawing.Size(75, 23)
        Me.btnprevious.TabIndex = 16
        Me.btnprevious.Text = "<<"
        Me.btnprevious.UseVisualStyleBackColor = True
        '
        'label17
        '
        Me.label17.AutoSize = True
        Me.label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label17.ForeColor = System.Drawing.Color.White
        Me.label17.Location = New System.Drawing.Point(9, 174)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(69, 16)
        Me.label17.TabIndex = 9
        Me.label17.Text = "Week off"
        '
        'label22
        '
        Me.label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label22.ForeColor = System.Drawing.Color.Black
        Me.label22.Location = New System.Drawing.Point(54, 0)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(50, 19)
        Me.label22.TabIndex = 8
        Me.label22.Text = "Mon"
        Me.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label23
        '
        Me.label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label23.ForeColor = System.Drawing.Color.Black
        Me.label23.Location = New System.Drawing.Point(207, 0)
        Me.label23.Name = "label23"
        Me.label23.Size = New System.Drawing.Size(50, 19)
        Me.label23.TabIndex = 11
        Me.label23.Text = "Thu"
        Me.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label24
        '
        Me.label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label24.ForeColor = System.Drawing.Color.Black
        Me.label24.Location = New System.Drawing.Point(105, 0)
        Me.label24.Name = "label24"
        Me.label24.Size = New System.Drawing.Size(50, 19)
        Me.label24.TabIndex = 9
        Me.label24.Text = "Tue"
        Me.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label25
        '
        Me.label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label25.ForeColor = System.Drawing.Color.Black
        Me.label25.Location = New System.Drawing.Point(156, 0)
        Me.label25.Name = "label25"
        Me.label25.Size = New System.Drawing.Size(50, 19)
        Me.label25.TabIndex = 10
        Me.label25.Text = "Wed"
        Me.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tabControl1
        '
        Me.tabControl1.Controls.Add(Me.tabReg)
        Me.tabControl1.Controls.Add(Me.tabPer)
        Me.tabControl1.Location = New System.Drawing.Point(2, 44)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(420, 295)
        Me.tabControl1.TabIndex = 22
        '
        'tabReg
        '
        Me.tabReg.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabReg.Controls.Add(Me.cmbshift3)
        Me.tabReg.Controls.Add(Me.Label10)
        Me.tabReg.Controls.Add(Me.cmbshift2)
        Me.tabReg.Controls.Add(Me.Label7)
        Me.tabReg.Controls.Add(Me.cmbShift1)
        Me.tabReg.Controls.Add(Me.Label2)
        Me.tabReg.Controls.Add(Me.btndays)
        Me.tabReg.Controls.Add(Me.checkBox1)
        Me.tabReg.Controls.Add(Me.txtDaysreg)
        Me.tabReg.Controls.Add(Me.label18)
        Me.tabReg.Controls.Add(Me.CmbShiftTypeReg)
        Me.tabReg.Controls.Add(Me.label16)
        Me.tabReg.Controls.Add(Me.cmbShiftnamereg)
        Me.tabReg.Controls.Add(Me.label9)
        Me.tabReg.Controls.Add(Me.txtNamereg)
        Me.tabReg.Controls.Add(Me.label8)
        Me.tabReg.Controls.Add(Me.cmbMacnoReg)
        Me.tabReg.Controls.Add(Me.label6)
        Me.tabReg.Location = New System.Drawing.Point(4, 22)
        Me.tabReg.Name = "tabReg"
        Me.tabReg.Padding = New System.Windows.Forms.Padding(3)
        Me.tabReg.Size = New System.Drawing.Size(412, 269)
        Me.tabReg.TabIndex = 0
        Me.tabReg.Text = "Regular"
        '
        'cmbshift3
        '
        Me.cmbshift3.BackColor = System.Drawing.Color.White
        Me.cmbshift3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbshift3.ForeColor = System.Drawing.Color.Black
        Me.cmbshift3.FormattingEnabled = True
        Me.cmbshift3.Location = New System.Drawing.Point(137, 223)
        Me.cmbshift3.Name = "cmbshift3"
        Me.cmbshift3.Size = New System.Drawing.Size(183, 24)
        Me.cmbshift3.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(11, 226)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 16)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Shift 3"
        '
        'cmbshift2
        '
        Me.cmbshift2.BackColor = System.Drawing.Color.White
        Me.cmbshift2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbshift2.ForeColor = System.Drawing.Color.Black
        Me.cmbshift2.FormattingEnabled = True
        Me.cmbshift2.Location = New System.Drawing.Point(137, 191)
        Me.cmbshift2.Name = "cmbshift2"
        Me.cmbshift2.Size = New System.Drawing.Size(183, 24)
        Me.cmbshift2.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(11, 196)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 16)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Shift 2"
        '
        'cmbShift1
        '
        Me.cmbShift1.BackColor = System.Drawing.Color.White
        Me.cmbShift1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShift1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbShift1.ForeColor = System.Drawing.Color.Black
        Me.cmbShift1.FormattingEnabled = True
        Me.cmbShift1.Location = New System.Drawing.Point(137, 160)
        Me.cmbShift1.Name = "cmbShift1"
        Me.cmbShift1.Size = New System.Drawing.Size(183, 24)
        Me.cmbShift1.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(11, 164)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 16)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Shift 1"
        '
        'btndays
        '
        Me.btndays.BackColor = System.Drawing.Color.White
        Me.btndays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndays.Location = New System.Drawing.Point(326, 129)
        Me.btndays.Name = "btndays"
        Me.btndays.Size = New System.Drawing.Size(75, 28)
        Me.btndays.TabIndex = 6
        Me.btndays.Text = "Edit"
        Me.btndays.UseVisualStyleBackColor = False
        '
        'checkBox1
        '
        Me.checkBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkBox1.ForeColor = System.Drawing.Color.White
        Me.checkBox1.Location = New System.Drawing.Point(326, 92)
        Me.checkBox1.Name = "checkBox1"
        Me.checkBox1.Size = New System.Drawing.Size(90, 41)
        Me.checkBox1.TabIndex = 14
        Me.checkBox1.Text = "Auto rotate Shift type"
        Me.checkBox1.UseVisualStyleBackColor = True
        '
        'txtDaysreg
        '
        Me.txtDaysreg.BackColor = System.Drawing.Color.White
        Me.txtDaysreg.Enabled = False
        Me.txtDaysreg.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDaysreg.ForeColor = System.Drawing.Color.Black
        Me.txtDaysreg.Location = New System.Drawing.Point(137, 130)
        Me.txtDaysreg.MaxLength = 2
        Me.txtDaysreg.Name = "txtDaysreg"
        Me.txtDaysreg.Size = New System.Drawing.Size(183, 24)
        Me.txtDaysreg.TabIndex = 12
        '
        'label18
        '
        Me.label18.AutoSize = True
        Me.label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label18.ForeColor = System.Drawing.Color.White
        Me.label18.Location = New System.Drawing.Point(8, 131)
        Me.label18.Name = "label18"
        Me.label18.Size = New System.Drawing.Size(129, 16)
        Me.label18.TabIndex = 11
        Me.label18.Text = "Shift routine Days"
        '
        'CmbShiftTypeReg
        '
        Me.CmbShiftTypeReg.BackColor = System.Drawing.Color.White
        Me.CmbShiftTypeReg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbShiftTypeReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbShiftTypeReg.ForeColor = System.Drawing.Color.Black
        Me.CmbShiftTypeReg.FormattingEnabled = True
        Me.CmbShiftTypeReg.Location = New System.Drawing.Point(137, 100)
        Me.CmbShiftTypeReg.Name = "CmbShiftTypeReg"
        Me.CmbShiftTypeReg.Size = New System.Drawing.Size(183, 24)
        Me.CmbShiftTypeReg.TabIndex = 8
        '
        'label16
        '
        Me.label16.AutoSize = True
        Me.label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label16.ForeColor = System.Drawing.Color.White
        Me.label16.Location = New System.Drawing.Point(8, 103)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(78, 16)
        Me.label16.TabIndex = 3
        Me.label16.Text = "Shift Type"
        '
        'cmbShiftnamereg
        '
        Me.cmbShiftnamereg.BackColor = System.Drawing.Color.White
        Me.cmbShiftnamereg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShiftnamereg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbShiftnamereg.ForeColor = System.Drawing.Color.Black
        Me.cmbShiftnamereg.FormattingEnabled = True
        Me.cmbShiftnamereg.Location = New System.Drawing.Point(137, 70)
        Me.cmbShiftnamereg.Name = "cmbShiftnamereg"
        Me.cmbShiftnamereg.Size = New System.Drawing.Size(183, 24)
        Me.cmbShiftnamereg.TabIndex = 7
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.ForeColor = System.Drawing.Color.White
        Me.label9.Location = New System.Drawing.Point(8, 73)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(83, 16)
        Me.label9.TabIndex = 6
        Me.label9.Text = "Shift Name"
        '
        'txtNamereg
        '
        Me.txtNamereg.BackColor = System.Drawing.Color.White
        Me.txtNamereg.Enabled = False
        Me.txtNamereg.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamereg.ForeColor = System.Drawing.Color.Black
        Me.txtNamereg.Location = New System.Drawing.Point(137, 40)
        Me.txtNamereg.Name = "txtNamereg"
        Me.txtNamereg.Size = New System.Drawing.Size(183, 24)
        Me.txtNamereg.TabIndex = 5
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.ForeColor = System.Drawing.Color.White
        Me.label8.Location = New System.Drawing.Point(6, 44)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(123, 16)
        Me.label8.TabIndex = 4
        Me.label8.Text = "Employee Name"
        '
        'cmbMacnoReg
        '
        Me.cmbMacnoReg.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbMacnoReg.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbMacnoReg.BackColor = System.Drawing.Color.White
        Me.cmbMacnoReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMacnoReg.ForeColor = System.Drawing.Color.Black
        Me.cmbMacnoReg.FormattingEnabled = True
        Me.cmbMacnoReg.Location = New System.Drawing.Point(137, 10)
        Me.cmbMacnoReg.Name = "cmbMacnoReg"
        Me.cmbMacnoReg.Size = New System.Drawing.Size(183, 24)
        Me.cmbMacnoReg.TabIndex = 3
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.ForeColor = System.Drawing.Color.White
        Me.label6.Location = New System.Drawing.Point(8, 13)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(106, 16)
        Me.label6.TabIndex = 3
        Me.label6.Text = "Employee No."
        '
        'tabPer
        '
        Me.tabPer.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tabPer.Controls.Add(Me.txtEmpNameper)
        Me.tabPer.Controls.Add(Me.cmbShiftNamePer)
        Me.tabPer.Controls.Add(Me.label5)
        Me.tabPer.Controls.Add(Me.label4)
        Me.tabPer.Controls.Add(Me.cmbMacNoPer)
        Me.tabPer.Controls.Add(Me.label3)
        Me.tabPer.Location = New System.Drawing.Point(4, 22)
        Me.tabPer.Name = "tabPer"
        Me.tabPer.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPer.Size = New System.Drawing.Size(412, 269)
        Me.tabPer.TabIndex = 1
        Me.tabPer.Text = "Permenant"
        '
        'txtEmpNameper
        '
        Me.txtEmpNameper.BackColor = System.Drawing.Color.White
        Me.txtEmpNameper.Enabled = False
        Me.txtEmpNameper.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpNameper.ForeColor = System.Drawing.Color.Black
        Me.txtEmpNameper.Location = New System.Drawing.Point(135, 39)
        Me.txtEmpNameper.Name = "txtEmpNameper"
        Me.txtEmpNameper.Size = New System.Drawing.Size(183, 24)
        Me.txtEmpNameper.TabIndex = 2
        '
        'cmbShiftNamePer
        '
        Me.cmbShiftNamePer.BackColor = System.Drawing.Color.White
        Me.cmbShiftNamePer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbShiftNamePer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbShiftNamePer.ForeColor = System.Drawing.Color.Black
        Me.cmbShiftNamePer.FormattingEnabled = True
        Me.cmbShiftNamePer.Location = New System.Drawing.Point(135, 68)
        Me.cmbShiftNamePer.Name = "cmbShiftNamePer"
        Me.cmbShiftNamePer.Size = New System.Drawing.Size(183, 24)
        Me.cmbShiftNamePer.TabIndex = 3
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.ForeColor = System.Drawing.Color.White
        Me.label5.Location = New System.Drawing.Point(8, 72)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(83, 16)
        Me.label5.TabIndex = 6
        Me.label5.Text = "Shift Name"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.ForeColor = System.Drawing.Color.White
        Me.label4.Location = New System.Drawing.Point(6, 41)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(123, 16)
        Me.label4.TabIndex = 5
        Me.label4.Text = "Employee Name"
        '
        'cmbMacNoPer
        '
        Me.cmbMacNoPer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbMacNoPer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbMacNoPer.BackColor = System.Drawing.Color.White
        Me.cmbMacNoPer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbMacNoPer.ForeColor = System.Drawing.Color.Black
        Me.cmbMacNoPer.FormattingEnabled = True
        Me.cmbMacNoPer.Location = New System.Drawing.Point(135, 10)
        Me.cmbMacNoPer.Name = "cmbMacNoPer"
        Me.cmbMacNoPer.Size = New System.Drawing.Size(183, 24)
        Me.cmbMacNoPer.TabIndex = 1
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.ForeColor = System.Drawing.Color.White
        Me.label3.Location = New System.Drawing.Point(7, 13)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(94, 16)
        Me.label3.TabIndex = 4
        Me.label3.Text = "Machine No."
        '
        'btnReport
        '
        Me.btnReport.BackColor = System.Drawing.Color.White
        Me.btnReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.Location = New System.Drawing.Point(559, 355)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(75, 28)
        Me.btnReport.TabIndex = 28
        Me.btnReport.Text = "Report"
        Me.btnReport.UseVisualStyleBackColor = False
        Me.btnReport.Visible = False
        '
        'cmbCompCode
        '
        Me.cmbCompCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCompCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCompCode.FormattingEnabled = True
        Me.cmbCompCode.Location = New System.Drawing.Point(132, 9)
        Me.cmbCompCode.Name = "cmbCompCode"
        Me.cmbCompCode.Size = New System.Drawing.Size(280, 23)
        Me.cmbCompCode.TabIndex = 29
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(12, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(118, 23)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "Company Code"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbLocCode
        '
        Me.cmbLocCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLocCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLocCode.FormattingEnabled = True
        Me.cmbLocCode.Location = New System.Drawing.Point(538, 9)
        Me.cmbLocCode.Name = "cmbLocCode"
        Me.cmbLocCode.Size = New System.Drawing.Size(249, 23)
        Me.cmbLocCode.TabIndex = 31
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(418, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 23)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Location Code"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnHoliday
        '
        Me.btnHoliday.BackColor = System.Drawing.Color.White
        Me.btnHoliday.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHoliday.Location = New System.Drawing.Point(74, 355)
        Me.btnHoliday.Name = "btnHoliday"
        Me.btnHoliday.Size = New System.Drawing.Size(157, 28)
        Me.btnHoliday.TabIndex = 33
        Me.btnHoliday.Text = "Holiday Master"
        Me.btnHoliday.UseVisualStyleBackColor = False
        '
        'FrmShiftRecording
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(793, 394)
        Me.Controls.Add(Me.btnHoliday)
        Me.Controls.Add(Me.cmbLocCode)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.cmbCompCode)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnReport)
        Me.Controls.Add(Me.btnclr)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.tabControl1)
        Me.ForeColor = System.Drawing.Color.Black
        Me.Name = "FrmShiftRecording"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Shift Recording"
        Me.panel1.ResumeLayout(False)
        Me.panel1.PerformLayout()
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl1.ResumeLayout(False)
        Me.tabReg.ResumeLayout(False)
        Me.tabReg.PerformLayout()
        Me.tabPer.ResumeLayout(False)
        Me.tabPer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents btnclr As System.Windows.Forms.Button
    Private WithEvents Cancel As System.Windows.Forms.Button
    Private WithEvents btnSave As System.Windows.Forms.Button
    Private WithEvents btnEdit As System.Windows.Forms.Button
    Private WithEvents panel1 As System.Windows.Forms.Panel
    Private WithEvents txtStartDay As System.Windows.Forms.TextBox
    Private WithEvents Label11 As System.Windows.Forms.Label
    Private WithEvents txtstartreg As System.Windows.Forms.TextBox
    Private WithEvents label19 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents label20 As System.Windows.Forms.Label
    Private WithEvents lblCurrentDate As System.Windows.Forms.Label
    Private WithEvents dataGridView1 As System.Windows.Forms.DataGridView
    Private WithEvents btnForward As System.Windows.Forms.Button
    Private WithEvents label21 As System.Windows.Forms.Label
    Private WithEvents cmbWeekReg As System.Windows.Forms.ComboBox
    Private WithEvents btnprevious As System.Windows.Forms.Button
    Private WithEvents label17 As System.Windows.Forms.Label
    Private WithEvents label22 As System.Windows.Forms.Label
    Private WithEvents label23 As System.Windows.Forms.Label
    Private WithEvents label24 As System.Windows.Forms.Label
    Private WithEvents label25 As System.Windows.Forms.Label
    Private WithEvents tabControl1 As System.Windows.Forms.TabControl
    Private WithEvents tabReg As System.Windows.Forms.TabPage
    Friend WithEvents cmbshift3 As System.Windows.Forms.ComboBox
    Private WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbshift2 As System.Windows.Forms.ComboBox
    Private WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbShift1 As System.Windows.Forms.ComboBox
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents btndays As System.Windows.Forms.Button
    Private WithEvents checkBox1 As System.Windows.Forms.CheckBox
    Private WithEvents txtDaysreg As System.Windows.Forms.TextBox
    Private WithEvents label18 As System.Windows.Forms.Label
    Private WithEvents CmbShiftTypeReg As System.Windows.Forms.ComboBox
    Private WithEvents label16 As System.Windows.Forms.Label
    Private WithEvents cmbShiftnamereg As System.Windows.Forms.ComboBox
    Private WithEvents label9 As System.Windows.Forms.Label
    Private WithEvents txtNamereg As System.Windows.Forms.TextBox
    Private WithEvents label8 As System.Windows.Forms.Label
    Private WithEvents cmbMacnoReg As System.Windows.Forms.ComboBox
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents tabPer As System.Windows.Forms.TabPage
    Private WithEvents txtEmpNameper As System.Windows.Forms.TextBox
    Private WithEvents cmbShiftNamePer As System.Windows.Forms.ComboBox
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents cmbMacNoPer As System.Windows.Forms.ComboBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents btnReport As System.Windows.Forms.Button
    Friend WithEvents cmbCompCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbLocCode As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents btnHoliday As System.Windows.Forms.Button
End Class

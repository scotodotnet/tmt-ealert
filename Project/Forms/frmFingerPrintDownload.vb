﻿Imports System
Imports System.Data
Imports System.Data.OleDb

Public Class frmFingerPrintDownload

    Dim Download_Machine_IP() As String
    Dim Upload_Machine_IP() As String
    Private bIsConnected = False
    Private iMachineNumber As Integer
    Public axCZKEM1 As New zkemkeeper.CZKEM
    Dim prifixCheck As Boolean = False
    Private Sub frmFingerPrintDownload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
        With gvsearch
            .Columns(0).Width = 65 'Sno
            .Columns(1).Width = 80 'Token No
            .Columns(2).Width = 186
            .Columns(3).Width = 65

        End With


        'cmbCompCode.SelectedIndex = 0
        'cmbCompCode.Enabled = False
        'cmbCompCode_LostFocus(sender, e)
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub cmbLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(cmbLocCode.Text) = "" Then Exit Sub

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        SSQL = ""
        SSQL = "Select ShiftDesc,StartIN ,StartIN_Days,EndIN_Days,StartOUT,StartOUT_days,EndOUT,EndOUT_Days"
        SSQL = SSQL & " From Shift_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr1(0) & "'"
        SSQL = SSQL & " and LocCode='" & iStr2(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbShifDesc.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbShifDesc.Items.Add(.Rows(iRow)("ShiftDesc"))
                Next
            End If
        End With
    End Sub
    Private Sub cmbLocCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub btnDownloadTmp9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownloadTmp9.Click

        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        If Trim(cmbCompCode.Text) = "" Then MsgBox("Select COMPANY NAME", MsgBoxStyle.Critical) : cmbCompCode.Focus() : Exit Sub
        If Trim(cmbLocCode.Text) = "" Then MsgBox("Select LOCATION NAME", MsgBoxStyle.Critical) : cmbLocCode.Focus() : Exit Sub
        gvsearch.Rows.Clear()

        SSQL = "select ExistingCode,EmpName,OTHours from ManOT_Details where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' and convert(datetime,OTDate,105) = convert(datetime,'" & txtAttnDate.Text & "',105) and convert(datetime,OTDateStr,105) = convert(datetime,'" & txtAttnDate.Text & "',105)"
        mDataSet = ReturnMultipleValue(SSQL)


        If mDataSet.Tables(0).Rows.Count <> 0 Then
            For i = 0 To mDataSet.Tables(0).Rows.Count - 1
                With gvsearch
                    .Rows.Add()
                    .Rows(.Rows.Count - 1).Cells(0).Value = .Rows.Count
                    .Rows(.Rows.Count - 1).Cells(1).Value = mDataSet.Tables(0).Rows(i)("ExistingCode").ToString()
                    .Rows(.Rows.Count - 1).Cells(2).Value = mDataSet.Tables(0).Rows(i)("EmpName").ToString()
                    .Rows(.Rows.Count - 1).Cells("OT_Hours").Value = mDataSet.Tables(0).Rows(i)("OTHours").ToString()

                End With
            Next
            btnDownloadTmp9.Enabled = False

        Else
            btnDownloadTmp9.Enabled = True

        End If
    End Sub
    Private Sub Spinning_Department_Load()
        Dim i As Int32
        Dim iStr1() As String
        Dim iStr2() As String
        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        gvsearch.Rows.Clear()

        SSQL = "select ExistingCode,(FirstName + '.' + MiddleInitial) as Name from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' and EmpPrefix='" & cmbPrefix.Text & "' and IsActive='Yes' order by ExistingCode asc "

        'SSQL = "select ExistingCode,(FirstName + '.' + MiddleInitial) as Name from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' and Wages='" & cmbWages.Text & "' and IsActive='Yes' order by ExistingCode asc "

        mDataSet = ReturnMultipleValue(SSQL)
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            For i = 0 To mDataSet.Tables(0).Rows.Count - 1
                With gvsearch
                    .Rows.Add()
                    .Rows(.Rows.Count - 1).Cells(0).Value = .Rows.Count
                    .Rows(.Rows.Count - 1).Cells(1).Value = mDataSet.Tables(0).Rows(i)("ExistingCode").ToString()
                    .Rows(.Rows.Count - 1).Cells(2).Value = mDataSet.Tables(0).Rows(i)("Name").ToString()
                    .Rows(.Rows.Count - 1).Cells("OT_Hours").Value = "0"
                End With
            Next
        End If

    End Sub
    Private Sub btnBatchUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatchUpdate.Click
        Dim Month As String
        Dim value As Integer
        Dim EmpNo As String
        Dim MachineID As String

        Dim i As Int32
        Dim iStr1() As String
        Dim iStr2() As String

        Month = Format(txtAttnDate.Value.Date, "MMMM").ToString()

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")

        If btnDownloadTmp9.Enabled = "False" Then

            For i = 0 To gvsearch.Rows.Count - 1

                With gvsearch

                    'Get ExiistingCode and MachineID from EmployeeDetails

                    SSQL1 = ""
                    SSQL1 = SSQL1 & "select EmpNo,MachineID from Employee_Mst where ExistingCode='" & .Rows(i).Cells(1).Value & "'  and CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
                    mDataSet = ReturnMultipleValue(SSQL1)

                    If mDataSet.Tables(0).Rows.Count <= 0 Then
                        Exit For
                    End If
                    EmpNo = mDataSet.Tables(0).Rows(0)("EmpNo").ToString()
                    MachineID = mDataSet.Tables(0).Rows(0)("MachineID").ToString()

                    'Update

                    SSQL = ""
                    SSQL = SSQL & "delete from ManOT_Details where MachineID='" & MachineID & "' and OTDate='" & txtAttnDate.Text & "'"




                    'SSQL = ""
                    'SSQL = SSQL & "update  ManOT_Details set EmpName='" & .Rows(i).Cells(2).Value & "',OTHours='" & .Rows(i).Cells("OT_Hours").Value & "'"
                    'SSQL = SSQL & "where ExistingCode='" & .Rows(i).Cells(1).Value & "' and OTDate='" & txtAttnDate.Text & "' and OTDateStr='" & txtAttnDate.Text & "'"




                    'SSQL = SSQL & "EmpName,OTHours,OTDate,OTDateStr,Enter_Date)"
                    'SSQL = SSQL & "values('" & iStr1(0) & "','" & iStr2(0) & "','" & cmbPrefix.Text & "', '" & EmpNo & "',"
                    'SSQL = SSQL & ",'" & MachineID & "','" & .Rows(i).Cells(2).Value & "','" & .Rows(i).Cells("OT_Hours").Value & "',"
                    'SSQL = SSQL & "'" & txtAttnDate.Text & "','" & txtAttnDate.Text & "','" & CDate(Now) & "')"

                    InsertDeleteUpdate(SSQL)

                    SSQL = ""
                    SSQL = SSQL & "insert into ManOT_Details(CompCode,LocCode,EmpPrefix,EmpNo,ExistingCode,MachineID,"
                    SSQL = SSQL & "EmpName,OTHours,OTDate,OTDateStr,Enter_Date)"
                    SSQL = SSQL & "values('" & iStr1(0) & "','" & iStr2(0) & "','" & cmbPrefix.Text & "', '" & EmpNo & "',"
                    SSQL = SSQL & "'" & .Rows(i).Cells(1).Value & "','" & MachineID & "','" & .Rows(i).Cells(2).Value & "','" & .Rows(i).Cells("OT_Hours").Value & "',"
                    SSQL = SSQL & "'" & txtAttnDate.Text & "','" & txtAttnDate.Text & "','" & CDate(Now) & "')"
                    InsertDeleteUpdate(SSQL)



                    'MsgBox("Manual OT. Updated Successfully", MsgBoxStyle.Exclamation)
                    'btnClear_Click(sender, e)

                End With


            Next

           
        Else
            For i = 0 To gvsearch.Rows.Count - 1
                With gvsearch
                    value = .Rows(i).Cells("OT_Hours").Value

                    If value = "0" Then
                        'Skip zero hours 

                    Else

                        'Get ExiistingCode and MachineID from EmployeeDetails
                        SSQL = ""
                        SSQL = SSQL & "select EmpNo,MachineID from Employee_Mst where ExistingCode='" & .Rows(i).Cells(1).Value & "'  and CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "'"
                        mDataSet = ReturnMultipleValue(SSQL)
                        EmpNo = mDataSet.Tables(0).Rows(0)("EmpNo").ToString()
                        MachineID = mDataSet.Tables(0).Rows(0)("MachineID").ToString()



                        'Insert Manual OT to manual_OT table
                        SSQL = ""
                        SSQL = SSQL & "insert into ManOT_Details(CompCode,LocCode,EmpPrefix,EmpNo,ExistingCode,MachineID,"
                        SSQL = SSQL & "EmpName,OTHours,OTDate,OTDateStr,Enter_Date)"
                        SSQL = SSQL & "values('" & iStr1(0) & "','" & iStr2(0) & "','" & cmbPrefix.Text & "', '" & EmpNo & "',"
                        SSQL = SSQL & "'" & .Rows(i).Cells(1).Value & "','" & MachineID & "','" & .Rows(i).Cells(2).Value & "','" & .Rows(i).Cells("OT_Hours").Value & "',"
                        SSQL = SSQL & "'" & txtAttnDate.Text & "','" & txtAttnDate.Text & "','" & CDate(Now) & "')"
                        InsertDeleteUpdate(SSQL)

                    End If
                End With
            Next

            MsgBox("Manual OT. Inserted Successfully", MsgBoxStyle.Exclamation)
            btnClear_Click(sender, e)
        End If
    End Sub
    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        'Dim kalyan As String
        Close()
    End Sub

    Private Sub cmbPrefix_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPrefix.SelectedIndexChanged

        If prifixCheck = False Then

            If Trim(cmbCompCode.Text) = "" Then MsgBox("Select COMPANY NAME", MsgBoxStyle.Critical) : cmbCompCode.Focus() : Exit Sub
            If Trim(cmbLocCode.Text) = "" Then MsgBox("Select LOCATION NAME", MsgBoxStyle.Critical) : cmbLocCode.Focus() : Exit Sub
            If Trim(cmbPrefix.Text) = "" Then MsgBox("Select Prefix", MsgBoxStyle.Critical) : cmbPrefix.Focus() : Exit Sub


            Call Spinning_Department_Load()
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        cmbPrefix.SelectedIndex = 0
        cmbCompCode.Text = ""
        cmbLocCode.Text = ""
        gvsearch.Rows.Clear()
        btnDownloadTmp9.Enabled = True
        txtName.Text = ""
    End Sub
    Private Sub txtName_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.LostFocus


    End Sub
    Private Sub txtTimeIN_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        Dim i As Int32
        Dim iStr1() As String
        Dim iStr2() As String

        iStr1 = Split(cmbCompCode.Text, " | ")
        iStr2 = Split(cmbLocCode.Text, " | ")
        gvsearch.Rows.Clear()


        SSQL = ""
        SSQL = "select ExistingCode,(FirstName + '.' + MiddleInitial) as Name,EmpPrefix from Employee_Mst where CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' and IsActive='Yes' and FirstName like '" & txtName.Text & "%' order by ExistingCode asc"
        mDataSet = ReturnMultipleValue(SSQL)
        prifixCheck = False
        If mDataSet.Tables(0).Rows.Count <> 0 Then
            For i = 0 To mDataSet.Tables(0).Rows.Count - 1
                prifixCheck = True
                With gvsearch
                    .Rows.Add()
                    .Rows(.Rows.Count - 1).Cells(0).Value = .Rows.Count
                    .Rows(.Rows.Count - 1).Cells(1).Value = mDataSet.Tables(0).Rows(i)("ExistingCode").ToString()
                    .Rows(.Rows.Count - 1).Cells(2).Value = mDataSet.Tables(0).Rows(i)("Name").ToString()
                    .Rows(.Rows.Count - 1).Cells("OT_Hours").Value = "0"
                    cmbPrefix.Text = mDataSet.Tables(0).Rows(i)("EmpPrefix").ToString()

                End With
            Next
        End If
        prifixCheck = False
    End Sub
End Class
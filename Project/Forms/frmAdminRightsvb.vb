﻿Public Class frmAdminRightsvb
    Dim mStatus As Long
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Dim mStrComp() As String
    Dim mStrLoc() As String
    Dim EnpPwd As String
    Dim sData As DataSet
    Dim EnpConPwd As String
    Private Sub frmAdminRightsvb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Trim(cmbCompCode.Text) = "" Then MsgBox("Select Company Name", MsgBoxStyle.Critical) : cmbCompCode.Focus() : Exit Sub
        If Trim(cmbLocCode.Text) = "" Then MsgBox("Select Location Name", MsgBoxStyle.Critical) : cmbLocCode.Focus() : Exit Sub

        If Trim(txtUserName.Text) = "" Then MsgBox("Enter User Name", MsgBoxStyle.Critical) : txtUserName.Focus() : Exit Sub
        If Trim(txtUserPwd.Text) = "" Then MsgBox("Enter Password", MsgBoxStyle.Critical) : txtUserPwd.Focus() : Exit Sub
        If Trim(txtUserConPwd.Text) = "" Then MsgBox("Enter Conform Password", MsgBoxStyle.Critical) : txtUserConPwd.Focus() : Exit Sub


        iStr1 = Split(Trim(cmbCompCode.Text), " | ")
        iStr2 = Split(Trim(cmbLocCode.Text), " | ")

        EnpPwd = Encryption(txtUserPwd.Text)
        EnpConPwd = Encryption(txtUserPwd.Text)


        SSQL = ""
        SSQL = SSQL & "Delete from Admin_Login where Ccode='" & iStr1(0) & "' and Lcode='" & iStr2(0) & "'"
        mStatus = InsertDeleteUpdate(SSQL)


        SSQL = ""
        SSQL = SSQL & "Insert into Admin_Login(UserName,Password,ConformPassword,AntPassword,Ccode,Lcode)"
        SSQL = SSQL & "Values('" & txtUserName.Text.Trim() & "','" & EnpPwd.Trim() & "','" & EnpConPwd.Trim() & "','" & txtUserPwd.Text.Trim() & "','" & iStr1(0) & "','" & iStr2(0) & "')"
        mStatus = InsertDeleteUpdate(SSQL)

        If mStatus > 0 Then
            MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Clear_Screen_Details()
            Exit Sub
        End If

        txtUserConPwd.Text = ""
        txtUserName.Text = ""
        txtUserPwd.Text = ""
        sData = ReturnMultipleValue(SSQL)

    End Sub

    Private Sub cmbCompCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub cmbCompCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCompCode.SelectedIndexChanged

    End Sub

    Private Sub txtUserConPwd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserConPwd.TextChanged

    End Sub
End Class
﻿Public Class frmLocation
    Dim mDataSet As New DataSet
    Dim mStatus As Long
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Trim(cmbCompCode.Text) = "" Then
                MessageBox.Show("Please Enter Company Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cmbCompCode.Focus()
                Exit Sub
            End If

            If Trim(txtLocCode.Text) = "" Then
                MessageBox.Show("Please Enter Location Code.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtLocCode.Focus()
                Exit Sub
            End If

            If Trim(txtLocName.Text) = "" Then
                MessageBox.Show("Please Enter Location Name.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtLocName.Focus()
                Exit Sub
            End If

            Dim mStr() As String

            mStr = Split(Trim(cmbCompCode.Text), " | ")

            SSQL = ""
            SSQL = "Delete from Location_Mst Where CompCode='" & mStr(0) & "'"
            SSQL = SSQL & " And LocCode='" & UCase(Remove_Single_Quote(Trim(txtLocCode.Text))) & "'"
            mStatus = InsertDeleteUpdate(SSQL)

            If mStatus >= 0 Then
                SSQL = ""
                SSQL = "Insert into Location_Mst values( "
                SSQL = SSQL & "'" & mStr(0) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtLocCode.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtLocName.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAdd1.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtAdd2.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCity.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtPincode.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtState.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtCountry.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef1.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef2.Text))) & "',"
                SSQL = SSQL & "'" & UCase(Remove_Single_Quote(Trim(txtUserDef3.Text))) & "')"
                mStatus = InsertDeleteUpdate(SSQL)
                If mStatus > 0 Then
                    MessageBox.Show("Successfully Saved.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Clear_Screen_Details()
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Altius")
        End Try
    End Sub
    Public Sub Clear_Screen_Details()
        txtLocCode.Text = ""
        txtLocName.Text = ""
        txtAdd1.Text = ""
        txtAdd2.Text = ""
        txtCity.Text = ""
        txtPincode.Text = ""
        txtState.Text = ""
        txtCountry.Text = ""
        txtUserDef1.Text = ""
        txtUserDef2.Text = ""
        txtUserDef3.Text = ""
        txtLocName.Focus()
    End Sub
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Clear_Screen_Details()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub frmLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormAlign(Me)
        Fill_Company_Details()
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name] from Company_Mst Order By CompName"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbCompCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub

    Private Sub txtLocCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLocCode.LostFocus
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        If Trim(txtLocCode.Text) = "" Then Exit Sub

        Dim mStr() As String

        mStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = "Select * from Location_Mst Where CompCode='" & mStr(0) & "'"
        SSQL = SSQL & " And LocCode='" & UCase(Remove_Single_Quote(Trim(txtLocCode.Text))) & "'"

        mDataSet = New DataSet
        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If (.Rows.Count) > 0 Then
                txtLocName.Text = .Rows(0)("LocName")
                txtAdd1.Text = .Rows(0)("Add1")
                txtAdd2.Text = .Rows(0)("Add2")
                txtCity.Text = .Rows(0)("City")
                txtPincode.Text = .Rows(0)("PinCode")
                txtState.Text = .Rows(0)("State")
                txtCountry.Text = .Rows(0)("country")
                txtUserDef1.Text = .Rows(0)("UserDef1")
                txtUserDef2.Text = .Rows(0)("UserDef2")
                txtUserDef3.Text = .Rows(0)("UserDef3")
                txtLocName.Focus()
                Exit Sub
            End If
        End With
    End Sub
End Class
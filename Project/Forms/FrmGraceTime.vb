﻿#Region "Imports"
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
#End Region
Public Class FrmGraceTime
    Dim mStatus As Long
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim iStr1() As String
    Dim iStr2() As String
    Dim iStr3() As String
    Dim mStrComp() As String
    Dim mStrLoc() As String

    Private Sub FrmGraceTime_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Fill_Company_Details()
        cmbCompCode.SelectedIndex = 0
        If Trim(mUserLocation) <> "" Then
            cmbLocCode.SelectedIndex = cmbLocCode.FindString(Trim(mUserLocation))
            cmbLocCode.Enabled = True
        End If
        ShiftTime_Grid()
       
        LCode()


    End Sub
    Private Sub butSave_ShTime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave_ShTime.Click
        Dim Errflag As Boolean = False

        If txtTime_ShTime.Text = "" Then
            MessageBox.Show("Select ShiftType.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Errflag = True
        End If
        If Errflag = False Then
            Dim SSQL As String = ""
            SSQL = "insert into Shiftmst(CompCode,LocCode,StartTime,ToTime,mintus)values('" & cmbCompCode.Text & "','" & cmbLocCode.Text & "','" & txtTime_ShTime.Text & "','" & txtTime_TTime.Text & "','" & txtMint_ShTime.Text & "') "
            Dim mSaveStatus As Long
            mSaveStatus = InsertDeleteUpdate(SSQL)
            If mSaveStatus > 0 Then
                ShiftTime_Grid()
            End If
        End If
    End Sub
    Public Sub Fill_Company_Details()
        SSQL = ""
        SSQL = "Select CompCode + ' | ' + CompName as [Name],CompCode from Company_Mst Order By CompName"
        cmbCompCode.Items.Clear()
        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbCompCode.DataSource = mDataSet.Tables(0)
                cmbCompCode.DisplayMember = "[Name]"
                cmbCompCode.ValueMember = "CompCode"
                'cmbCompCode.Items.Clear()
                'For iRow = 0 To .Rows.Count - 1
                '    cmbCompCode.Items.Add(.Rows(iRow)(0))
                'Next
            End If
        End With
    End Sub
    Public Sub LCode()
        If Trim(cmbCompCode.Text) = "" Then Exit Sub
        Dim iStr() As String
        iStr = Split(Trim(cmbCompCode.Text), " | ")

        SSQL = ""
        SSQL = "Select LocCode + ' | ' + LocName as [Name] from Location_Mst Where Compcode='"
        SSQL = SSQL & "" & iStr(0) & "'"

        mDataSet = ReturnMultipleValue(SSQL)

        With mDataSet.Tables(0)
            If .Rows.Count > 0 Then
                cmbLocCode.Items.Clear()
                For iRow = 0 To .Rows.Count - 1
                    cmbLocCode.Items.Add(.Rows(iRow)(0))
                Next
            End If
        End With
    End Sub
    Public Sub ShiftTime_Grid()
        Grd_ShiftTime.Rows.Clear()
        mDataSet = New DataSet
        SSQL = ""
        SSQL = "Select StartTime,ToTime,mintus from Shiftmst Order By ShiftDesc"

        mDataSet = ReturnMultipleValue(SSQL)
        With mDataSet.Tables(0)
            For j = 0 To mDataSet.Tables(0).Rows.Count - 1

                With Grd_ShiftTime
                    .Rows.Add()
                    .Rows(.Rows.Count - 1).Cells(0).Value = mDataSet.Tables(0).Rows(j)("StartTime").ToString()
                    .Rows(.Rows.Count - 1).Cells(1).Value = mDataSet.Tables(0).Rows(j)("ToTime").ToString()
                    .Rows(.Rows.Count - 1).Cells(2).Value = mDataSet.Tables(0).Rows(j)("mintus").ToString()


                End With

            Next


        End With

    End Sub

End Class